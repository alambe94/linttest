/**
 * @file l6470_thread.c
 * @brief Implements L6470 ISR
 * @author medprime (www.medprimetech.com)
 * @version 0.0.0
 **/
#include "l6470_thread.h"
#include "motor_api.h"
#include "lens_detection.h"
#include "microscope_config.h"
#include "L6470.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/**
 * @addtogroup Motor_Control
 * @{
 */

/**
 * @defgroup L6470_ISR_Define
 * @{
 */

#define L6470_TASK_STACK_SIZE 256u
#define L6470_TASK_PRIORITY 6u

/**
 * @}
 */
/* End of L6470_ISR_Define */

/**
 * @defgroup L6470_ISR_externs
 * @{
 */

extern TaskHandle_t Print_Done_Task_Handle;

/**
 * @}
 */
/* End of L6470_ISR_externs */

/**
 * @defgroup L6470_ISR_Globals
 * @{
 */

TaskHandle_t L6470_Task_Handle;
StackType_t L6470_Task_Stack[L6470_TASK_STACK_SIZE];
StaticTask_t L6470_Task_TCB;

SemaphoreHandle_t L6470_Access_Mutex;
StaticSemaphore_t L6470_Access_Mutex_Buffer;

/* Semaphore for  synchronization */
SemaphoreHandle_t L6470_Motion_SEM;
StaticSemaphore_t L6470_Motion_SEM_Buffer;

SemaphoreHandle_t L6470_Idle_SEM;
StaticSemaphore_t L6470_Idle_SEM_Buffer;

/**
 * @}
 */
/* End of L6470_ISR_Globals */

/**
 * @defgroup L6470_ISR_Prototypes
 * @{
 */
static void L6470_TASK(void *argument);

/**
 * @}
 */
/* End of L6470_ISR_Prototypes */

/**
 * @defgroup L6470_ISR_Functions
 * @{
 */

/**
 * @brief create task to handle l6470 ISR
 **/
void L6470_Thread_Add()
{
    L6470_Access_Mutex = xSemaphoreCreateMutexStatic(&L6470_Access_Mutex_Buffer);

    L6470_Motion_SEM = xSemaphoreCreateBinaryStatic(&L6470_Motion_SEM_Buffer);

    L6470_Idle_SEM = xSemaphoreCreateBinaryStatic(&L6470_Idle_SEM_Buffer);

    L6470_Task_Handle = xTaskCreateStatic(L6470_TASK,
                                          "L6470_TASK",
                                          L6470_TASK_STACK_SIZE,
                                          NULL,
                                          L6470_TASK_PRIORITY,
                                          L6470_Task_Stack,
                                          &L6470_Task_TCB);
}

/**
 * @brief entry point of application.
 * configures motors drivers, perform homing cycle and handle l6470 interrups
 * @param FreeRTOS argument
 **/
void L6470_TASK(void *argument)
{
    /* gaurd l6470 */
    xSemaphoreTake(L6470_Access_Mutex, portMAX_DELAY);

    Motor_Config();
    Lens_Color_Sensor_Config();

    uint16_t status_register_x;
    uint16_t status_register_y;
    uint16_t status_register_z;
    uint16_t status_register_m;

#if (HOMING_CYCLE == 1)
    /* refresh status register*/
    status_register_x = L6470_GetStatus(X_AXIS_INDEX);
    status_register_y = L6470_GetStatus(Y_AXIS_INDEX);
    status_register_z = L6470_GetStatus(Z_AXIS_INDEX);
    status_register_m = L6470_GetStatus(M_AXIS_INDEX);

    uint32_t speed = Step_s_2_Speed(1000);

    if (status_register_z & STATUS_SW_F)
    {
        L6470_ReleaseSW(Z_AXIS_INDEX, L6470_ACT_RST_ID, L6470_DIR_FWD_ID);
    }
    else
    {
        L6470_GoUntil(Z_AXIS_INDEX, L6470_ACT_RST_ID, L6470_DIR_REV_ID, speed);
    }
    /* wait for the z homing*/
    xSemaphoreTake(L6470_Idle_SEM, 5000);

    /** scan all lenses connected to microscope */
    Lens_Scan_All();

    if (status_register_x & STATUS_SW_F)
    {
        L6470_ReleaseSW(X_AXIS_INDEX, L6470_ACT_RST_ID, L6470_DIR_FWD_ID);
    }
    else
    {
        L6470_GoUntil(X_AXIS_INDEX, L6470_ACT_RST_ID, L6470_DIR_REV_ID, speed);
    }

    if (status_register_y & STATUS_SW_F)
    {
        L6470_ReleaseSW(Y_AXIS_INDEX, L6470_ACT_RST_ID, L6470_DIR_FWD_ID);
    }
    else
    {
        L6470_GoUntil(Y_AXIS_INDEX, L6470_ACT_RST_ID, L6470_DIR_REV_ID, speed);
    }

    /* wait for the x and y homing*/
    xSemaphoreTake(L6470_Idle_SEM, 5000);
#else
    /** suppress lint warning about scope */
    (void)status_register_x;
    (void)status_register_y;
    (void)status_register_z;
    (void)status_register_m;
#endif

    while (1)
    {
        /* release l6470 */
        xSemaphoreGive(L6470_Access_Mutex);

        /* wait for l6470 interrupt or .. */
        ulTaskNotifyTake(pdTRUE, pdMS_TO_TICKS(portMAX_DELAY));

        /* gaurd l6470 */
        xSemaphoreTake(L6470_Access_Mutex, portMAX_DELAY);

        status_register_x = L6470_GetStatus(X_AXIS_INDEX);
        status_register_y = L6470_GetStatus(Y_AXIS_INDEX);
        status_register_z = L6470_GetStatus(Z_AXIS_INDEX);
        status_register_m = L6470_GetStatus(M_AXIS_INDEX);

        /* check for stall flag */
        if ((status_register_x & STATUS_STEP_LOSS_A) == 0 || (status_register_x & STATUS_STEP_LOSS_B) == 0)
        {
            L6470_HardStop(X_AXIS_INDEX);
        }
        if ((status_register_y & STATUS_STEP_LOSS_A) == 0 || (status_register_y & STATUS_STEP_LOSS_B) == 0)
        {
            L6470_HardStop(Y_AXIS_INDEX);
        }
        if ((status_register_z & STATUS_STEP_LOSS_A) == 0 || (status_register_z & STATUS_STEP_LOSS_B) == 0)
        {
            L6470_HardStop(Z_AXIS_INDEX);
        }
        if ((status_register_m & STATUS_STEP_LOSS_A) == 0 || (status_register_m & STATUS_STEP_LOSS_B) == 0)
        {
            L6470_HardStop(M_AXIS_INDEX);
        }

        /* check for limit switch flag */
        if (status_register_x & STATUS_SW_EVN)
        {
            if (status_register_x & STATUS_SW_F)
            {
                L6470_ReleaseSW(X_AXIS_INDEX, L6470_ACT_RST_ID, L6470_DIR_FWD_ID);
            }
            else
            {
                /** home sequence complete */
            }
        }

        if (status_register_y & STATUS_SW_EVN)
        {
            if (status_register_y & STATUS_SW_F)
            {
                L6470_ReleaseSW(Y_AXIS_INDEX, L6470_ACT_RST_ID, L6470_DIR_FWD_ID);
            }
            else
            {
                /** home sequence complete */
            }
        }

        if (status_register_z & STATUS_SW_EVN)
        {
            if (status_register_z & STATUS_SW_F)
            {
                L6470_ReleaseSW(Z_AXIS_INDEX, L6470_ACT_RST_ID, L6470_DIR_FWD_ID);
            }
            else
            {
                /** home sequence complete */
            }
        }

        if (status_register_m & STATUS_SW_EVN)
        {
            if (status_register_m & STATUS_SW_F)
            {
                L6470_ReleaseSW(M_AXIS_INDEX, L6470_ACT_RST_ID, L6470_DIR_FWD_ID);
            }
            else
            {
                /** home sequence complete */
            }
        }
    }
}

/**
 * @brief L6470_Busy_SYNC_INT_Pin ISR
 **/
void L6470_Busy_SYNC_ISR()
{
    BaseType_t xHigherPriorityTaskWoken;

    /*L6470_BUSY_SYNC_INT_Pin is low when motion cmd is executing, generates rising edge/high whene motion is completed (idle)*/
    /*L6470_BUSY_SYNC_INT_Pin is configured to generate interrupt on rising edge @see gpio.c*/
    /*giving this semaphore indicate motor is no longer in motion*/

    xSemaphoreGiveFromISR(L6470_Idle_SEM, &xHigherPriorityTaskWoken);

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 * @brief L6470_Flag_INT_Pin ISR
 **/
void L6470_Flag_ISR()
{
    BaseType_t xHigherPriorityTaskWoken;

    /*L6470_Flag_INT_Pin generates falling edge interrupt when event occurs ie stall, switch and others event (idle state is high)*/
    /*L6470 INT flag is cleared by reading L6470 status register*/
    /*L6470_Flag_INT_Pin is configured to generate interrupt on falling edge @see gpio.c*/

    vTaskNotifyGiveFromISR(L6470_Task_Handle, &xHigherPriorityTaskWoken);

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 * @}
 */
/* End of L6470_ISR_Functions */

/**
 * @}
 */
/* End of Motor_Control */
