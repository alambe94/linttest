/** st includes */
#include "L6470.h"

/** app includes */
#include "motor_config.h"
#include "microscope_config.h"

/**
 * @brief move the stepper to given position (relative)
 * @param axis_index axis to move
 * @param micron number of micron to move
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Move(uint8_t axis_index, float micron)
{
    uint8_t direction = L6470_DIR_FWD_ID;

    if (micron < 0)
    {
        micron = -1 * micron;
        direction = L6470_DIR_REV_ID;
        if (Motor_Get_Current_Position(axis_index) - micron < 0)
        {
            micron = Motor_Get_Current_Position(axis_index);
        }
    }
#if (ENABLE_TRAVEL_LIMIT == 1)
    else
    {
        if (Motor_Get_Current_Position(axis_index) + micron > Motor_Get_MAX_Limit(axis_index))
        {
            micron = Motor_Get_MAX_Limit(axis_index) - Motor_Get_Current_Position(axis_index);
        }
    }
#endif

    /** convert micrometer to steps */
    uint32_t steps = Motor_Get_Micro_Steps_Per_Micron(axis_index) * micron;

    L6470_PrepareMove(axis_index, direction, steps);

    return 1;
}

/**
 * @brief move the stepper to given position (absolute)
 * @param axis_index axis to move
 * @param micron number of micron to move
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Goto(uint8_t axis_index, float micron)
{
    uint8_t direction = L6470_DIR_FWD_ID;

    if (micron > 0)
    {
#if (ENABLE_TRAVEL_LIMIT == 1)
        if (micron > Motor_Get_MAX_Limit(axis_index))
        {
            micron = Motor_Get_MAX_Limit(axis_index);
        }
#endif
        if (micron < Motor_Get_Current_Position(axis_index))
        {
            direction = L6470_DIR_REV_ID; //reverse
        }
    }
    else
    {
        micron = 0;
    }

    /** convert micrometer to micro steps */
    uint32_t steps = Motor_Get_Micro_Steps_Per_Micron(axis_index) * micron;

    L6470_PrepareGoToDir(axis_index, direction, steps);

    return 1;
}

/**
 * @brief run the stepper at given speed
 * @param axis_index axis to run
 * @param param running speed (um/sec)
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Run(uint8_t axis_index, float micron_per_sec)
{
    uint8_t direction = L6470_DIR_FWD_ID;

    if (micron_per_sec < 0)
    {
        micron_per_sec = -1 * micron_per_sec;
        direction = L6470_DIR_REV_ID;
    }

#if (ENABLE_TRAVEL_LIMIT == 1)
    if (Motor_Get_Current_Position(axis_index) > Motor_Get_MAX_Limit(axis_index))
    {
        micron_per_sec = 0;
    }
    if (Motor_Get_Current_Position(axis_index) < 0)
    {
        micron_per_sec = 0;
    }
#endif
    /** convert micron/sec to setps/sec */
    uint32_t speed = Motor_Get_Full_Steps_Per_Micron(axis_index) * micron_per_sec;

    /** convert steps/sec to appropriate register value */
    speed = Step_s_2_Speed(speed);

    L6470_PrepareRun(axis_index, direction, speed);

    return 1;
}

/**
 * @brief begin homing sequence for given axis
 * @param axis_index axis to home
 * @param param optional speed value of homing cycle
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Home(uint8_t axis_index, float homing_speed)
{
    uint8_t xreturn = 1;

    /* speed val must be positive*/
    if (homing_speed < 0)
    {
        xreturn = 0;
    }
    else
    {
        if (0 == (int32_t)homing_speed)
        {
            /** if homing speed is not specified use default */
            homing_speed = 10000;
        }

        /** convert micron/sec to steps/sec */
        homing_speed = Motor_Get_Full_Steps_Per_Micron(axis_index) * homing_speed;

        /** convert value to appropriate register value */
        homing_speed = Step_s_2_Speed(homing_speed);

        /* read switch status if already pressed, issue release command*/
        /* after release, reset absolute position register*/
        if (L6470_GetStatus(axis_index) & STATUS_SW_F)
        {
            L6470_PrepareReleaseSW(axis_index,
                                   L6470_ACT_RST_ID,
                                   L6470_DIR_FWD_ID);
        }
        /* read switch status if not pressed, issue go command*/
        /* after click, reset absolute position register*/
        else
        {
            L6470_PrepareGoUntil(axis_index,
                                 L6470_ACT_RST_ID,
                                 L6470_DIR_REV_ID,
                                 homing_speed);
        }
    }

    return xreturn;
}

/**
 * @brief set current speed register in L6470
 * @param axis_index selected axis
 * @param param micron/sec
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_Speed(uint8_t axis_index, float speed)
{
    uint8_t xreturn = 1;

    /* speed val must be positive*/
    if (speed < 0)
    {
        xreturn = 0;
    }
    else
    {
        /** micron/sec to steps/sec */
        speed = Motor_Get_Full_Steps_Per_Micron(axis_index) * speed;

        if (speed > 15625)
        {
            speed = 15625;
        }

        /** convert value to appropriate register value */
        speed = Step_s_2_Speed(speed);

        L6470_PrepareSetParam(axis_index, L6470_SPEED_ID, speed);
    }

    return xreturn;
}

/**
 * @brief set min speed value in L6470
 * @param axis_index selected axis
 * @param speed
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_Min_Speed(uint8_t axis_index, float speed)
{
    uint8_t xreturn = 1;

    /* speed val must be positive*/
    if (speed < 0)
    {
        xreturn = 0;
    }
    else
    {
        /** micron/sec to steps/sec */
        speed = Motor_Get_Full_Steps_Per_Micron(axis_index) * speed;

        if (speed > 976)
        {
            speed = 976;
        }

        /** convert value to appropriate register value */
        speed = Step_s_2_MinSpeed(speed);

        L6470_PrepareSetParam(axis_index, L6470_MIN_SPEED_ID, speed);
    }

    return xreturn;
}

/**
 * @brief set max speed value in L6470
 * @param axis_index selected axis
 * @param speed to be set
 * @retval return 1 if success
 */
uint8_t Axis_Set_Max_Speed(uint8_t axis_index, float speed)
{
    uint8_t xreturn = 1;

    /* speed val must be positive*/
    if (speed < 0)
    {
        xreturn = 0;
    }
    else
    {
        /** micron/sec to steps/sec */
        speed = Motor_Get_Full_Steps_Per_Micron(axis_index) * speed;

        if (speed > 15610)
        {
            speed = 15610;
        }

        if (speed < 16)
        {
            speed = 16;
        }

        /** convert value to appropriate register value */
        speed = Step_s_2_MaxSpeed(speed);

        L6470_PrepareSetParam(axis_index, L6470_MAX_SPEED_ID, speed);
    }

    return xreturn;
}

/**
 * @brief set fs speed value in L6470
 * @param axis_index selected axis
 * @param speed to be set
 * @retval return 1 if success
 */
uint8_t Axis_Set_FS_Speed(uint8_t axis_index, float speed)
{
    uint8_t xreturn = 1;

    /* speed val must be positive*/
    if (speed < 0)
    {
        xreturn = 0;
    }
    else
    {
        /** micron/sec to steps/sec */
        speed = Motor_Get_Full_Steps_Per_Micron(axis_index) * speed;

        if (speed > 15625)
        {
            speed = 15625;
        }

        if (speed < 8)
        {
            speed = 8;
        }

        /** convert value to appropriate register value */
        speed = Step_s_2_MaxSpeed(speed);

        L6470_PrepareSetParam(axis_index, L6470_FS_SPD_ID, speed);
    }

    return xreturn;
}

/**
 * @brief set acc value in L6470
 * @param axis_index selected axis
 * @param acc
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_ACC(uint8_t axis_index, float acc)
{
    uint8_t xreturn = 1;

    /* acc val must be positive*/
    if (acc < 0)
    {
        xreturn = 0;
    }
    else
    {
        /** micron/sec/sec to steps/sec/sec */
        acc = Motor_Get_Full_Steps_Per_Micron(axis_index) * acc;

        if (acc > 59590)
        {
            acc = 59590;
        }

        if (acc < 15)
        {
            acc = 15;
        }

        /** convert value to appropriate register value */
        acc = Step_s2_2_Acc(acc);

        L6470_PrepareSetParam(axis_index, L6470_ACC_ID, acc);
    }

    return xreturn;
}

/**
 * @brief set dec value in L6470
 * @param axis_index selected axis
 * @param dec
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_DEC(uint8_t axis_index, float dec)
{
    uint8_t xreturn = 1;

    /* dec val must be positive*/
    if (dec < 0)
    {
        xreturn = 0;
    }
    else
    {
        /** micron/sec/sec to setps/sec/sec */
        dec = Motor_Get_Full_Steps_Per_Micron(axis_index) * dec;

        if (dec > 59590)
        {
            dec = 59590;
        }

        if (dec < 15)
        {
            dec = 15;
        }

        /** convert value to appropriate register value */
        dec = Step_s2_2_Dec(dec);

        L6470_PrepareSetParam(axis_index, L6470_DEC_ID, dec);
    }

    return xreturn;
}

/**
 * @brief et fs spd value in L6470
 * @param axis_index selected axis
 * @param fs_speed
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_FS_SPD(uint8_t axis_index, float fs_speed)
{
    uint8_t xreturn = 1;

    /* fs_speed val must be positive*/
    if (fs_speed < 0)
    {
        xreturn = 0;
    }
    else
    {
        /** micron/sec/sec to setps/sec/sec */
        fs_speed = Motor_Get_Full_Steps_Per_Micron(axis_index) * fs_speed;

        if (fs_speed > 15625)
        {
            fs_speed = 15625;
        }

        if (fs_speed < 8)
        {
            fs_speed = 8;
        }

        /** convert value to appropriate register value */
        fs_speed = Step_s_2_FsSpd(fs_speed);

        L6470_PrepareSetParam(axis_index, L6470_FS_SPD_ID, fs_speed);
    }

    return xreturn;
}

/**
 * @brief set kval hold value in L6470
 * @param axis_index selected axis
 * @param kval
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_KVAL_Hold(uint8_t axis_index, float kval)
{
    uint8_t xreturn = 1;

    /* kval must be positive*/
    if (kval < 0)
    {
        xreturn = 0;
    }
    else
    {
        if (kval > V_MOTOR)
        {
            kval = V_MOTOR;
        }

        /** convert value to appropriate register value */
        kval = (uint8_t)((kval * 256) / V_MOTOR);

        L6470_PrepareSetParam(axis_index, L6470_KVAL_HOLD_ID, kval);
    }

    return xreturn;
}

/**
 * @brief set kval run value in L6470
 * @param axis_index selected axis
 * @param kval
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_KVAL_Run(uint8_t axis_index, float kval)
{
    uint8_t xreturn = 1;

    /* kval must be positive*/
    if (kval < 0)
    {
        xreturn = 0;
    }
    else
    {
        if (kval > V_MOTOR)
        {
            kval = V_MOTOR;
        }

        /** convert value to appropriate register value */
        kval = (uint8_t)((kval * 256) / V_MOTOR);

        L6470_PrepareSetParam(axis_index, L6470_KVAL_RUN_ID, kval);
    }

    return xreturn;
}

/**
 * @brief set kval acc value in L6470
 * @param axis_index selected axis
 * @param kval
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_KVAL_ACC(uint8_t axis_index, float kval)
{
    uint8_t xreturn = 1;

    /* kval must be positive*/
    if (kval < 0)
    {
        xreturn = 0;
    }
    else
    {
        if (kval > V_MOTOR)
        {
            kval = V_MOTOR;
        }

        /** convert value to appropriate register value */
        kval = (uint8_t)((kval * 256) / V_MOTOR);

        L6470_PrepareSetParam(axis_index, L6470_KVAL_ACC_ID, kval);
    }

    return xreturn;
}

/**
 * @brief set kval dec value in L6470
 * @param axis_index selected axis
 * @param kval
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_KVAL_DEC(uint8_t axis_index, float kval)
{
    uint8_t xreturn = 1;

    /* kval must be positive*/
    if (kval < 0)
    {
        xreturn = 0;
    }
    else
    {
        if (kval > V_MOTOR)
        {
            kval = V_MOTOR;
        }

        /** convert value to appropriate register value */
        kval = (uint8_t)((kval * 256) / V_MOTOR);

        L6470_PrepareSetParam(axis_index, L6470_KVAL_DEC_ID, kval);
    }

    return xreturn;
}

/**
 * @brief set over current threshold value in L6470
 * @param axis_index selected axis
 * @param ocd
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_OCD_TH(uint8_t axis_index, float ocd)
{
    uint8_t xreturn = 1;

    /* ocd must be positive*/
    if (ocd < 0)
    {
        xreturn = 0;
    }
    else
    {
        if (ocd > 6000)
        {
            ocd = 6000;
        }

        if (ocd < 375)
        {
            ocd = 375;
        }

        /** convert value to appropriate register value */
        ocd = mA_2_OcdTh(ocd);

        L6470_PrepareSetParam(axis_index, L6470_OCD_TH_ID, ocd);
    }

    return xreturn;
}

/**
 * @brief set stall threshold value in L6470
 * @param axis_index selected axis
 * @param stall
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_Stall_TH(uint8_t axis_index, float stall)
{
    uint8_t xreturn = 1;

    /* stall must be positive*/
    if (stall < 0)
    {
        xreturn = 0;
    }
    else
    {
        if (stall > 4000)
        {
            stall = 4000;
        }

        if (stall < 31)
        {
            stall = 31;
        }

        /** convert value to appropriate register value */
        stall = mA_2_StallTh(stall);

        L6470_PrepareSetParam(axis_index, L6470_STALL_TH_ID, stall);
    }

    return xreturn;
}

/**
 * @brief set microstep value in L6470
 * @param axis_index selected axis
 * @param val
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_Microstep(uint8_t axis_index, float val)
{
    uint8_t xreturn = 1;

    /* val must be positive*/
    if (val < 0)
    {
        xreturn = 0;
    }
    else
    {
        uint8_t microstep = (uint8_t)val;

        if (microstep > 128)
        {
            microstep = 128;
        }

        switch (microstep)
        {
        case 0:
            microstep = 0x00;
            break;
        case 2:
            microstep = 0x01;
            break;
        case 4:
            microstep = 0x02;
            break;
        case 8:
            microstep = 0x03;
            break;
        case 16:
            microstep = 0x04;
            break;
        case 32:
            microstep = 0x05;
            break;
        case 64:
            microstep = 0x06;
            break;
        case 128:
            microstep = 0x07;
            break;
        default:
            break;
        }

        L6470_PrepareSetParam(axis_index, L6470_STEP_MODE_ID, microstep);
    }

    return xreturn;
}

/**
 * @brief set config register in L6470
 * @param axis_index selected axis
 * @param param value of configuration register
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Set_Config(uint8_t axis_index, float val)
{
    uint8_t xreturn = 1;

    /* val must be positive*/
    if (val < 0)
    {
        xreturn = 0;
    }
    else
    {
        uint8_t config = (uint16_t)val;
        L6470_PrepareSetParam(axis_index, L6470_CONFIG_ID, config);
    }

    return xreturn;
}

/**
 * @brief set given stepper motor in high impedance mode
 * @param axis_index selected axis
 * @param param not used, only axis name is sufficient
 * @retval xreturn return 1 if success
 */
uint8_t Axis_Release(uint8_t axis_index, float val)
{
    L6470_PrepareHardHiZ(axis_index);
    return 1;
}

