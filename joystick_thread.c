/**
 * @file joystic.c
 * @brief implement joystic related funtions
 * @author medprime (www.medprimetech.com)
 * @version 0.0.0
 **/

/** system includes */
#include <stdio.h>
#include <math.h>

/** st includes */
#include "L6470.h"
#include "usart.h"

/** RTOS includes */
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

/** app includes */
#include "joystick_thread.h"
#include "microscope_config.h"
#include "cli_uart_interface.h"
#include "motor_api.h"
#include "ring_buffer.h"

/**
 * @addtogroup Joystick
 * @{
 */

/**
 * @defgroup Joystick_Defines
 * @{
 */

#define JOYSTICK_RING_BUFFER_SIZE 128

#define JOYSTICK_TASK_STACK_SIZE 256u
#define JOYSTICK_TASK__PRIORITY 3u

#define SEND_POSITION_TASK_STACK_SIZE 256u
#define SEND_POSITION_TASK__PRIORITY 2u

/**
 * @}
 */
/* End of Joystick_Defines */

/**
 * @defgroup Joystick_Macros
 * @{
 */

/**
 * @brief data is written to buffer via uart DMA in background, need to update Write_Index manually
 **/

#define UPDATE_JOYSTICK_RING_BUFFER() (Joystick_Ring_Buffer_Handle.Write_Index = \
                                           (JOYSTICK_RING_BUFFER_SIZE -          \
                                            (Joystick_UART->hdmarx->Instance->CNDTR)))

/**
 * @}
 */
/* End of Joystick_Macros */

/**
 * @defgroup Joysstick_externs
 * @{
 */

extern SemaphoreHandle_t L6470_Access_Mutex;
extern SemaphoreHandle_t CLI_UART_Access_Mutex;
extern SemaphoreHandle_t CLI_UART_TX_CMPLT_SEM;

/*0-> manual, 1-> auto */
extern uint8_t Control_Mode;

/**
 * @}
 */
/* End of Joysstick_externs */

/**
 * @defgroup Joysstick_Gloabal_Variables
 * @{
 */

static uint8_t Joystick_Ring_Buffer[JOYSTICK_RING_BUFFER_SIZE];
static Ring_Buffer_t Joystick_Ring_Buffer_Handle;

UART_HandleTypeDef *Joystick_UART = &huart1;

static TaskHandle_t Joystick_Task_Handle;
static StackType_t Joystick_Task_Stack[JOYSTICK_TASK_STACK_SIZE];
static StaticTask_t Joystick_Task_TCB;

static TaskHandle_t Send_Position_Task_Handle;
static StackType_t Send_Position_Task_Stack[SEND_POSITION_TASK_STACK_SIZE];
static StaticTask_t Send_Position_Task_TCB;

static uint8_t Joystick_Mode[NO_OF_MOTORS] = {0, 1, 0, 0};
static uint16_t Joystick_Multiplier[NO_OF_MOTORS] = {10, 10, 10, 10};

/**
 * @brief Maxim APPLICATION NOTE 27
 * converted in hex
 **/
static uint8_t CRC_Table[] =
    {
        0x00, 0x5E, 0xBC, 0xE2, 0x61, 0x3F, 0xDD, 0x83, 0xC2, 0x9C, 0x7E, 0x20, 0xA3, 0xFD, 0x1F, 0x41,
        0x9D, 0xC3, 0x21, 0x7F, 0xFC, 0xA2, 0x40, 0x1E, 0x5F, 0x01, 0xE3, 0xBD, 0x3E, 0x60, 0x82, 0xDC,
        0x23, 0x7D, 0x9F, 0xC1, 0x42, 0x1C, 0xFE, 0xA0, 0xE1, 0xBF, 0x5D, 0x03, 0x80, 0xDE, 0x3C, 0x62,
        0xBE, 0xE0, 0x02, 0x5C, 0xDF, 0x81, 0x63, 0x3D, 0x7C, 0x22, 0xC0, 0x9E, 0x1D, 0x43, 0xA1, 0xFF,
        0x46, 0x18, 0xFA, 0xA4, 0x27, 0x79, 0x9B, 0xC5, 0x84, 0xDA, 0x38, 0x66, 0xE5, 0xBB, 0x59, 0x07,
        0xDB, 0x85, 0x67, 0x39, 0xBA, 0xE4, 0x06, 0x58, 0x19, 0x47, 0xA5, 0xFB, 0x78, 0x26, 0xC4, 0x9A,
        0x65, 0x3B, 0xD9, 0x87, 0x04, 0x5A, 0xB8, 0xE6, 0xA7, 0xF9, 0x1B, 0x45, 0xC6, 0x98, 0x7A, 0x24,
        0xF8, 0xA6, 0x44, 0x1A, 0x99, 0xC7, 0x25, 0x7B, 0x3A, 0x64, 0x86, 0xD8, 0x5B, 0x05, 0xE7, 0xB9,
        0x8C, 0xD2, 0x30, 0x6E, 0xED, 0xB3, 0x51, 0x0F, 0x4E, 0x10, 0xF2, 0xAC, 0x2F, 0x71, 0x93, 0xCD,
        0x11, 0x4F, 0xAD, 0xF3, 0x70, 0x2E, 0xCC, 0x92, 0xD3, 0x8D, 0x6F, 0x31, 0xB2, 0xEC, 0x0E, 0x50,
        0xAF, 0xF1, 0x13, 0x4D, 0xCE, 0x90, 0x72, 0x2C, 0x6D, 0x33, 0xD1, 0x8F, 0x0C, 0x52, 0xB0, 0xEE,
        0x32, 0x6C, 0x8E, 0xD0, 0x53, 0x0D, 0xEF, 0xB1, 0xF0, 0xAE, 0x4C, 0x12, 0x91, 0xCF, 0x2D, 0x73,
        0xCA, 0x94, 0x76, 0x28, 0xAB, 0xF5, 0x17, 0x49, 0x08, 0x56, 0xB4, 0xEA, 0x69, 0x37, 0xD5, 0x8B,
        0x57, 0x09, 0xEB, 0xB5, 0x36, 0x68, 0x8A, 0xD4, 0x95, 0xCB, 0x29, 0x77, 0xF4, 0xAA, 0x48, 0x16,
        0xE9, 0xB7, 0x55, 0x0B, 0x88, 0xD6, 0x34, 0x6A, 0x2B, 0x75, 0x97, 0xC9, 0x4A, 0x14, 0xF6, 0xA8,
        0x74, 0x2A, 0xC8, 0x96, 0x15, 0x4B, 0xA9, 0xF7, 0xB6, 0xE8, 0x0A, 0x54, 0xD7, 0x89, 0x6B, 0x35};

/**
 * @}
 */
/* End of Joysstick_Gloabal_Variables */

/**
 * @defgroup Joysstick_Protypes
 * @{
 */

static uint8_t CRC8(uint8_t *data, uint8_t len);
static void Joystick_Task(void *argument);
static void Send_Position_Task(void *argument);
static void Joystick_Run_Helper(uint8_t index, int16_t steps);
static void Joystick_Move_Helper(uint8_t index, int16_t steps);

/**
 * @}
 */
/* End of Joysstick_Protypes */

/**
 * @brief calculate crc8 on input buffer
 * @param data input buffer
 * @param len number bytes in input buffer
 * @retval crc reeturn the calculated crc
 **/
static uint8_t CRC8(uint8_t *data, uint8_t len)
{
    uint8_t crc = 0;

    for (uint8_t i = 0; i < len; i++)
    {
        crc = CRC_Table[crc ^ data[i]];
    }

    return crc;
}

/**
 * @brief create joystick and send position task
 **/
void Joystick_Thread_Add()
{
    Joystick_Task_Handle = xTaskCreateStatic(Joystick_Task,
                                             "Joystick_Task",
                                             JOYSTICK_TASK_STACK_SIZE,
                                             NULL,
                                             JOYSTICK_TASK__PRIORITY,
                                             Joystick_Task_Stack,
                                             &Joystick_Task_TCB);

    Send_Position_Task_Handle = xTaskCreateStatic(Send_Position_Task,
                                                  "Send_Position_Task",
                                                  SEND_POSITION_TASK_STACK_SIZE,
                                                  NULL,
                                                  SEND_POSITION_TASK__PRIORITY,
                                                  Send_Position_Task_Stack,
                                                  &Send_Position_Task_TCB);

    Ring_Buffer_Init(&Joystick_Ring_Buffer_Handle,
                     Joystick_Ring_Buffer,
                     1,
                     JOYSTICK_RING_BUFFER_SIZE);

    HAL_UART_Receive_DMA(Joystick_UART,
                         Joystick_Ring_Buffer,
                         JOYSTICK_RING_BUFFER_SIZE);

    /* Enable idle interrupt */
    __HAL_UART_ENABLE_IT(Joystick_UART, UART_IT_IDLE);
}

/**
 * @brief give run command to given stepper
 * @param index selected stepper
 * @param steps steps per second run velocity
 **/
static void Joystick_Run_Helper(uint8_t axis_index, int16_t speed)
{
    uint8_t direction = L6470_DIR_FWD_ID;
    int32_t speed_val = 0;

    if (speed < 0)
    {
        speed *= -1;
        direction = L6470_DIR_REV_ID;
    }

    speed_val = speed * exp(speed * Joystick_Multiplier[axis_index] / 50);

    speed_val = Step_s_2_Speed(speed_val);

    L6470_PrepareRun(axis_index, direction, speed_val);
}

/**
 * @brief give move command to given stepper
 * @param index selected stepper
 * @param steps steps to move
 **/
static void Joystick_Move_Helper(uint8_t axis_index, int16_t steps)
{
    uint8_t direction = L6470_DIR_FWD_ID;

    steps = (steps * Joystick_Multiplier[axis_index]);

    if (steps < 0)
    {
        steps = (-1) * steps;
        direction = L6470_DIR_REV_ID;
    }

    L6470_PrepareMove(axis_index, direction, steps);
}

/**
 * @brief set the specified joystic mode (0 or 1)
 * @param axis_index axis for which mode to be changed
 * @param param mode value 1 or 0, mode1-> move motion, mode1-> run motion(smooth but less accurate)
 * @retval xreturn return 1 if success
 * @see Joystick_Set_Mode()
 */
uint8_t Joystick_Set_Mode(uint8_t axis_index[], float mode[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            uint8_t val = (uint8_t)mode[i];

            /* currently there are only two joystick mode*/
            /* mode 0 is using move cmd*/
            /* mode 1 is using run cmd*/
            if (val > 1)
            {
                xreturn = 0;
            }
            else
            {
                Joystick_Mode[i] = val;
            }
        }
    }

    return xreturn;
}

/**
 * @brief get joystick mode for axis
 * @param axis selected stepper
 * @param mode 0-> run mode, 1-> move mode
 **/
uint8_t Joystick_Get_Mode(uint8_t mode[])
{
    mode[X_AXIS_INDEX] = Joystick_Mode[X_AXIS_INDEX];
    mode[Y_AXIS_INDEX] = Joystick_Mode[Y_AXIS_INDEX];
    mode[Z_AXIS_INDEX] = Joystick_Mode[Z_AXIS_INDEX];
    mode[M_AXIS_INDEX] = Joystick_Mode[M_AXIS_INDEX];

    return 1;
}

/**
 * @brief set the specified joystick multiplier
 * @param axis_index axis for which mode to be changed
 * @param param multiplier value 0 to 255
 * @retval xreturn return 1 if success
 * @see Joystick_Set_Multiplier()
 */
uint8_t Joystick_Set_Multiplier(uint8_t axis_index[], float mul[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            Joystick_Multiplier[i] = (uint16_t)mul[i];
        }
    }

    return xreturn;
}

/**
 * @brief get multiplier
 * @param axis selected stepper
 **/
uint8_t Joystick_Get_Multiplier(uint16_t mul[])
{
    mul[X_AXIS_INDEX] = Joystick_Multiplier[X_AXIS_INDEX];
    mul[Y_AXIS_INDEX] = Joystick_Multiplier[Y_AXIS_INDEX];
    mul[Z_AXIS_INDEX] = Joystick_Multiplier[Z_AXIS_INDEX];
    mul[M_AXIS_INDEX] = Joystick_Multiplier[M_AXIS_INDEX];

    return 1;
}

/**
 * @brief give move command to given stepper
 * @param argument FreeRTOS argument
 **/
void Joystick_Task(void *argument)
{
    uint8_t rx_char = 0;
    uint8_t data_frame[8];

    for (;;)
    {
        /*wait for idle interrupt*/
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        while (Ring_Buffer_Get_Count(&Joystick_Ring_Buffer_Handle))
        {
            /* pop char from ring buffer */
            Ring_Buffer_Get_Char(&Joystick_Ring_Buffer_Handle, &rx_char);

            /* look for delimiter */
            if (rx_char == '$')
            {
                data_frame[0] = rx_char;

                /* X axis */
                Ring_Buffer_Get_Char(&Joystick_Ring_Buffer_Handle, &data_frame[1]);
                Ring_Buffer_Get_Char(&Joystick_Ring_Buffer_Handle, &data_frame[2]);

                /* Y axis */
                Ring_Buffer_Get_Char(&Joystick_Ring_Buffer_Handle, &data_frame[3]);
                Ring_Buffer_Get_Char(&Joystick_Ring_Buffer_Handle, &data_frame[4]);

                /* Z axis */
                Ring_Buffer_Get_Char(&Joystick_Ring_Buffer_Handle, &data_frame[5]);
                Ring_Buffer_Get_Char(&Joystick_Ring_Buffer_Handle, &data_frame[6]);

                /* CRC */
                Ring_Buffer_Get_Char(&Joystick_Ring_Buffer_Handle, &data_frame[7]);

                if (data_frame[7] == CRC8(data_frame, 7))
                {
                    int16_t x_steps = (data_frame[1] << 8) | data_frame[2];

                    int16_t y_steps = (data_frame[3] << 8) | data_frame[4];

                    int16_t z_steps = (data_frame[5] << 8) | data_frame[6];

                    /* gaurd l6470 */
                    xSemaphoreTake(L6470_Access_Mutex, portMAX_DELAY);

                    if (Joystick_Mode[X_AXIS_INDEX])
                    {
                        Joystick_Move_Helper(X_AXIS_INDEX, x_steps);
                    }
                    else
                    {
                        Joystick_Run_Helper(X_AXIS_INDEX, x_steps);
                    }

                    if (Joystick_Mode[Y_AXIS_INDEX])
                    {
                        Joystick_Move_Helper(Y_AXIS_INDEX, y_steps);
                    }
                    else
                    {
                        Joystick_Run_Helper(Y_AXIS_INDEX, y_steps);
                    }

                    if (Joystick_Mode[Z_AXIS_INDEX])
                    {
                        Joystick_Move_Helper(Z_AXIS_INDEX, z_steps);
                    }
                    else
                    {
                        Joystick_Run_Helper(Z_AXIS_INDEX, z_steps);
                    }

                    L6470_PerformPreparedApplicationCommand();

                    /* Send_Position_Task that position has changed*/
                    xTaskNotifyGive(Send_Position_Task_Handle);

                    /* release l6470 */
                    xSemaphoreGive(L6470_Access_Mutex);
                }
            }
        }
    }
}

/**
 * @brief send current positions to console after movement change
 * @param argument FreeRTOS argument
 **/
static void Send_Position_Task(void *argument)
{
    char buffer[50];
    float pos[NO_OF_MOTORS];

    while (1)
    {
        vTaskDelay(1000);

        /* wait for notification */
        /* only send position if there is joystick activity*/
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        /* gaurd l6470 */
        xSemaphoreTake(L6470_Access_Mutex, portMAX_DELAY);

        /* gaurd uart */
        xSemaphoreTake(CLI_UART_Access_Mutex, portMAX_DELAY);

        Motor_Get_Position(pos);

        snprintf(buffer,
                 sizeof(buffer),
                 "x%0.4f\r\ny%0.4f\r\nz%0.4f\r\nm%0.4f\r\n",
                 pos[X_AXIS_INDEX],
                 pos[Y_AXIS_INDEX],
                 pos[Z_AXIS_INDEX],
                 pos[M_AXIS_INDEX]);

        CLI_UART_Send_String_DMA(buffer);

        /* wait for transmission to complete */
        xSemaphoreTake(CLI_UART_TX_CMPLT_SEM, portMAX_DELAY);

        /* release l6470 */
        xSemaphoreGive(L6470_Access_Mutex);

        /* release uart */
        xSemaphoreGive(CLI_UART_Access_Mutex);
    }
}

/**
 *
 * @brief joystick uart rx isr
 * @see USART1_IRQHandler
 **/
void Joystick_UART_RX_ISR()
{
    /* check idle flag if it triggered isr*/
    if (__HAL_UART_GET_FLAG(Joystick_UART, UART_FLAG_IDLE))
    {
        __HAL_UART_CLEAR_IDLEFLAG(Joystick_UART);

        /**
         * @brief data is written to buffer via uart DMA in background, need to update Write_Index manually
        **/
        UPDATE_JOYSTICK_RING_BUFFER();

        BaseType_t xHigherPriorityTaskWoken;
        vTaskNotifyGiveFromISR(Joystick_Task_Handle, &xHigherPriorityTaskWoken);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}

/**
 * @}
 */
/* End of Joystick */
