#include "stm32_hal.h"
#include "cli_uart_interface.h"
#include "l6470_thread.h"
#include "joystick_thread.h"
#include "FreeRTOSConfig.h"
#include "tim.h"

/**
 * @brief global firmware version string
 **/
const char Firmware_Version[] = "V3.2.0";

void Microscope_Init(void)
{
#if (TRC_USE_TRACEALYZER_RECORDER == 1)
    vTraceEnable(TRC_START);
#endif

#if (FLASH_BASE != 0x08004000UL)
#error "bootloader is enabled, must adjust FLASH_BASE in stm32f103xe.h/stm32f401xe.h"\
       "also update linker script STM32F103RETX_FLASH.ld/STM32F401RETX_FLASH.ld accordingly "\
       "0x08008000UL for 32K of bootloader size"\
       "0x08004000UL for 16K of bootloader size"
#endif

    /*Enable WLED PWM*/
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);

    CLI_UART_Thread_Add();

    L6470_Thread_Add();

    Joystick_Thread_Add();
}
