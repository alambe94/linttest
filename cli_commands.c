/**
 * @file CLI_commands.c
 * @brief Implements L6470 control commands
 * @author medprime (www.medprimetech.com)
 * @version 0.0.0
 * @note micron and micrometer are used interchangeably
 */

/** system includes */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

/** st includes */
#include "L6470.h"
#include "usart.h"
#include "i2c.h"
#include "tim.h"

/** RTOS includes */
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "timers.h"

/** app includes */
#include "cli_commands.h"
#include "cli_uart_interface.h"
#include "cli.h"
#include "lens_detection.h"
#include "motor_api.h"
#include "microscope_config.h"
#include "l6470_thread.h"
#include "joystick_thread.h"
#include "expression_parser.h"

/**
 * @addtogroup CLI_Commands
 * @{
 */

/**
 * @defgroup CLI_Defines
 * @{
 */

/** PWM TIM configured in cube @see tim.c*/
#define WLED_PWM_MAX 100
#define WLED_SET_BRIGHTNESS(x) __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_3, x)

#define PRINT_DONE_TASK_STACK_SIZE 128u
#define PRINT_DONE_TASK_PRIORITY 3u

/**
 * @}
 */
/* End of CLI_Defines */

/**
 * @defgroup CLI_extern_Variables
 * @{
 */

extern SemaphoreHandle_t CLI_UART_Access_Mutex;
extern SemaphoreHandle_t L6470_Idle_SEM;
extern SemaphoreHandle_t L6470_Motion_SEM;

extern const char Firmware_Version[];

/**
 * @}
 */
/* End of CLI_extern_Variables */

/**
 * @defgroup CLI_Global_Variables
 * @{
 */

uint8_t Ping_Enable_Flag = 0;
uint16_t Z_Axis_Min_Speed = 100; // reset

TaskHandle_t Print_Done_Task_Handle;
StaticTask_t Print_Done_Task_TCB;
StackType_t Print_Done_Task_Stack[PRINT_DONE_TASK_STACK_SIZE];

TimerHandle_t Ping_Timer_Handle;
StaticTimer_t Ping_Timer_TCB;

/**
 * @}
 */
/* End of CLI_Global_Variables */

/**
 * @defgroup CLI_Function_Protypes
 * @{
 */

/* function prototype for FreeRTOS task*/
static void Print_Done_Task(void *argument);

/* function prototype for FreeRTOS timer callback*/
static void Ping_Timer_Callback(TimerHandle_t xTimer);

static uint8_t Move_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Goto_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Run_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Home_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Get_Pos_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Scan_Lens_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Set_Lens_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_Lens_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_Lens_All_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Set_Speed_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_Speed_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Set_Min_Speed_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_Min_Speed_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Set_Max_Speed_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_Max_Speed_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Set_ACC_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_ACC_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Set_DEC_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_DEC_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Set_KVAL_Hold_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_KVAL_Hold_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Set_KVAL_Run_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_KVAL_Run_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Set_KVAL_ACC_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_KVAL_ACC_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Set_KVAL_DEC_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_KVAL_DEC_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Set_OCD_TH_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_OCD_TH_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Set_Stall_TH_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_Stall_TH_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Set_Microsteps_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_Microsteps_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Set_Config_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_Config_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Set_Brightness_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Set_Joystick_Mode_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_Joystick_Mode_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Set_Joystick_Multiplier_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
static uint8_t Get_Joystick_Multiplier_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Get_Version_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Release_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);

static uint8_t Update_Callback(uint8_t argc, const char *argv[], char *out_buffer, uint16_t out_max);
/**
 * @}
 */
/* End of CLI_Function_Protypes */

/**
 * @defgroup CLI_Commands_Definitions
 * @{
 */

CLI_Command_t Move_Definition =
    {
        .CLI_Command = "move",
        .CLI_Command_Description = "move stepper for given steps",
        .CLI_Callback = Move_Callback};

CLI_Command_t Goto_Definition =
    {
        .CLI_Command = "goto",
        .CLI_Command_Description = "move motor to absolute position",
        .CLI_Callback = Goto_Callback};

CLI_Command_t Run_Definition =
    {
        .CLI_Command = "run",
        .CLI_Command_Description = "run motor at given speed",
        .CLI_Callback = Run_Callback};

CLI_Command_t Home_Definition =
    {
        .CLI_Command = "home",
        .CLI_Command_Description = "goto home position",
        .CLI_Callback = Home_Callback};

CLI_Command_t Get_Pos_Definition =
    {
        .CLI_Command = "getpos",
        .CLI_Command_Description = "return current position of motors",
        .CLI_Callback = Get_Pos_Callback};

CLI_Command_t Scan_Lens_Definition =
    {
        .CLI_Command = "scanlens",
        .CLI_Command_Description = "scan all lens on microscope",
        .CLI_Callback = Scan_Lens_Callback};

CLI_Command_t Set_Lens_Definition =
    {
        .CLI_Command = "setlens",
        .CLI_Command_Description = "lens change",
        .CLI_Callback = Set_Lens_Callback};

CLI_Command_t Get_Lens_Definition =
    {
        .CLI_Command = "getlens",
        .CLI_Command_Description = "get current lens",
        .CLI_Callback = Get_Lens_Callback};

CLI_Command_t Get_Lens_All_Definition =
    {
        .CLI_Command = "getlensall",
        .CLI_Command_Description = "return all the available lenses on microscope",
        .CLI_Callback = Get_Lens_All_Callback};

CLI_Command_t Set_Speed_Definition =
    {
        .CLI_Command = "setspeed",
        .CLI_Command_Description = "set max speed of motor",
        .CLI_Callback = Set_Speed_Callback};

CLI_Command_t Get_Speed_Definition =
    {
        .CLI_Command = "getspeed",
        .CLI_Command_Description = "get current speed of motor",
        .CLI_Callback = Get_Speed_Callback};

CLI_Command_t Set_Min_Speed_Definition =
    {
        .CLI_Command = "setminspeed",
        .CLI_Command_Description = "set min speed of motor",
        .CLI_Callback = Set_Min_Speed_Callback};

CLI_Command_t Get_Min_Speed_Definition =
    {
        .CLI_Command = "getminspeed",
        .CLI_Command_Description = "get min speed of motor",
        .CLI_Callback = Get_Min_Speed_Callback};

CLI_Command_t Set_Max_Speed_Definition =
    {
        .CLI_Command = "setmaxspeed",
        .CLI_Command_Description = "set max speed of motor",
        .CLI_Callback = Set_Max_Speed_Callback};

CLI_Command_t Get_Max_Speed_Definition =
    {
        .CLI_Command = "getmaxspeed",
        .CLI_Command_Description = "get max speed of motor",
        .CLI_Callback = Get_Max_Speed_Callback};

CLI_Command_t Set_ACC_Definition =
    {
        .CLI_Command = "setacc",
        .CLI_Command_Description = "set acceleration of motor",
        .CLI_Callback = Set_ACC_Callback};

CLI_Command_t Get_ACC_Definition =
    {
        .CLI_Command = "getacc",
        .CLI_Command_Description = "get acceleration of motor",
        .CLI_Callback = Get_ACC_Callback};

CLI_Command_t Set_DEC_Definition =
    {
        .CLI_Command = "setdec",
        .CLI_Command_Description = "set deceleration of motor",
        .CLI_Callback = Set_DEC_Callback};

CLI_Command_t Get_DEC_Definition =
    {
        .CLI_Command = "getdec",
        .CLI_Command_Description = "get deceleration of motor",
        .CLI_Callback = Get_DEC_Callback};

CLI_Command_t Set_KVAL_Hold_Definition =
    {
        .CLI_Command = "setkvalhold",
        .CLI_Command_Description = "set holding torque of motor",
        .CLI_Callback = Set_KVAL_Hold_Callback};

CLI_Command_t Get_KVAL_Hold_Definition =
    {
        .CLI_Command = "getkvalhold",
        .CLI_Command_Description = "get holding torque of motor",
        .CLI_Callback = Get_KVAL_Hold_Callback};

CLI_Command_t Set_KVAL_Run_Definition =
    {
        .CLI_Command = "setkvalrun",
        .CLI_Command_Description = "set running torque of motor",
        .CLI_Callback = Set_KVAL_Run_Callback};

CLI_Command_t Get_KVAL_Run_Definition =
    {
        .CLI_Command = "getkvalrun",
        .CLI_Command_Description = "get running torque of motor",
        .CLI_Callback = Get_KVAL_Run_Callback};

CLI_Command_t Set_KVAL_ACC_Definition =
    {
        .CLI_Command = "setkvalacc",
        .CLI_Command_Description = "set accelerating torque of motor",
        .CLI_Callback = Set_KVAL_ACC_Callback};

CLI_Command_t Get_KVAL_ACC_Definition =
    {
        .CLI_Command = "getkvalacc",
        .CLI_Command_Description = "get accelerating torque of motor",
        .CLI_Callback = Get_KVAL_ACC_Callback};

CLI_Command_t Set_KVAL_DEC_Definition =
    {
        .CLI_Command = "setkvaldec",
        .CLI_Command_Description = "set decelerating torque of motor",
        .CLI_Callback = Set_KVAL_DEC_Callback};

CLI_Command_t Get_KVAL_DEC_Definition =
    {
        .CLI_Command = "getkvaldec",
        .CLI_Command_Description = "get decelerating torque of motor",
        .CLI_Callback = Get_KVAL_DEC_Callback};

CLI_Command_t Set_OCD_TH_Definition =
    {
        .CLI_Command = "setocdth",
        .CLI_Command_Description = "set over current threshold of motor",
        .CLI_Callback = Set_OCD_TH_Callback};

CLI_Command_t Get_OCD_TH_Definition =
    {
        .CLI_Command = "getocdth",
        .CLI_Command_Description = "get over current threshold of motor",
        .CLI_Callback = Get_OCD_TH_Callback};

CLI_Command_t Set_Stall_TH_Definition =
    {
        .CLI_Command = "setstallth",
        .CLI_Command_Description = "set stall current threshold of motor",
        .CLI_Callback = Set_Stall_TH_Callback};

CLI_Command_t Get_Stall_TH_Definition =
    {
        .CLI_Command = "getstallth",
        .CLI_Command_Description = "get stall current threshold of motor",
        .CLI_Callback = Get_Stall_TH_Callback};

CLI_Command_t Set_Microsteps_Definition =
    {
        .CLI_Command = "setmicrostep",
        .CLI_Command_Description = "set microstep of motor",
        .CLI_Callback = Set_Microsteps_Callback};

CLI_Command_t Get_Microsteps_Definition =
    {
        .CLI_Command = "getmicrostep",
        .CLI_Command_Description = "get microstep of motor",
        .CLI_Callback = Get_Microsteps_Callback};

CLI_Command_t Set_Config_Definition =
    {
        .CLI_Command = "setconfig",
        .CLI_Command_Description = "set config register of motor",
        .CLI_Callback = Set_Config_Callback};

CLI_Command_t Get_Config_Definition =
    {
        .CLI_Command = "getconfig",
        .CLI_Command_Description = "get config register of motor",
        .CLI_Callback = Get_Config_Callback};

CLI_Command_t Set_Brightness_Definition =
    {
        .CLI_Command = "setbrightness",
        .CLI_Command_Description = "set brightness of WLED",
        .CLI_Callback = Set_Brightness_Callback};

CLI_Command_t Set_Joystick_Mode_Definition =
    {
        .CLI_Command = "setjoystickmode",
        .CLI_Command_Description = "set joystick mode 0 or 1",
        .CLI_Callback = Set_Joystick_Mode_Callback};

CLI_Command_t Get_Joystick_Mode_Definition =
    {
        .CLI_Command = "getjoystickmode",
        .CLI_Command_Description = "get joystick mode of axis",
        .CLI_Callback = Get_Joystick_Mode_Callback};

CLI_Command_t Set_Joystick_Multiplier_Definition =
    {
        .CLI_Command = "setjoystickmul",
        .CLI_Command_Description = "set joystick multiplier",
        .CLI_Callback = Set_Joystick_Multiplier_Callback};

CLI_Command_t Get_Joystick_Multiplier_Definition =
    {
        .CLI_Command = "getjoystickmul",
        .CLI_Command_Description = "get joystick multiplier of axis",
        .CLI_Callback = Get_Joystick_Multiplier_Callback};

CLI_Command_t Get_Version_Definition =
    {
        .CLI_Command = "version",
        .CLI_Command_Description = "return the current version of firmware",
        .CLI_Callback = Get_Version_Callback};

CLI_Command_t Release_Definition =
    {
        .CLI_Command = "release",
        .CLI_Command_Description = "put stepper in high impedance",
        .CLI_Callback = Release_Callback};

CLI_Command_t Update_Definition =
    {
        .CLI_Command = "update",
        .CLI_Command_Description = "jump to bootloader to update firmware",
        .CLI_Callback = Update_Callback};

/**
 * @}
 */
/* End of CLI_Commands_Definitions */

/**
 * @defgroup Sprint_Helper_Functions
 * @{
 */

/**
 * @brief print "invalid command" in response to invalid input command
 * @param in received input command string
 * @param out output buffer, reply is written to this buffer
 * @param out_max maximum number bytes that can be written to out buffer
 */
void Sprint_Error_Helper(const char *in, char *out, uint16_t out_max)
{
    snprintf(out, out_max, "Error->%s invalid command\r\n", in);
}

/**
 * @brief print requested values in output buffer in response to input command (values in float)
 * @param argc argument count in input string
 * @param argv[] arguent list
 * @param out_buffer output buffer
 * @param out_max maximum number bytes that can be written to out buffer
 * @param val[] param values to be printed in output buffer
 */
void Sprint_Reply_Float_Helper(uint8_t argc,
                               const char *argv[],
                               char *out_buffer,
                               uint16_t out_max,
                               float val[])
{
    /* if axis is not specified then send values of all axis*/
    if (argc == 1)
    {
        snprintf(out_buffer,
                 out_max,
                 "Ok\r\n"
                 "x%0.4f\r\ny%0.4f\r\nz%0.4f\r\nm%0.4f\r\n"
                 "Done\r\n",
                 val[X_AXIS_INDEX],
                 val[Y_AXIS_INDEX],
                 val[Z_AXIS_INDEX],
                 val[M_AXIS_INDEX]);
    }
    /* send values of specified axis only*/
    else
    {
        uint8_t is_command_valid = 1;

        uint16_t out_written = snprintf(out_buffer, out_max, "%s", "Ok\r\n") - 1; /** exclude null termination */

        for (uint8_t i = 1; i < argc; i++)
        {
            if (CLI_Get_Argument_Length(argv[i]) == 1)
            {
                char axis = *argv[i];
                float temp = 0;

                switch (axis)
                {
                case 'x':
                case 'y':
                case 'z':
                case 'm':

                    temp = val[Motor_Name_To_Index(axis)];

                    out_written += snprintf(out_buffer + out_written,
                                            out_max - out_written,
                                            "%c%0.4f\r\n",
                                            axis,
                                            temp);

                    /** exclude null termination */
                    out_written--;

                    break;

                default:
                    is_command_valid = 0;
                    break;
                }
            }
            else
            {
                is_command_valid = 0;
            }
        }

        if (is_command_valid)
        {
            snprintf(out_buffer + out_written,
                     out_max - out_written,
                     "Done\r\n");
        }
        else
        {
            Sprint_Error_Helper(argv[0], out_buffer, out_max);
        }
    }
}

/**
 * @brief print requested values in output buffer in response to input command(values are integer)
 * @param argc argument count in input string
 * @param argv[] arguent list
 * @param out_buffer output buffer
 * @param out_max maximum number bytes that can be written to out buffer
 * @param val[] param values to be printed in output buffer
 */
void Sprint_Reply_Int_Helper(uint8_t argc,
                             const char *argv[],
                             char *out_buffer,
                             uint16_t out_max,
                             int32_t val[])
{
    /* if axis is not specified then send values of all axis*/
    if (argc == 1)
    {
        snprintf(out_buffer,
                 out_max,
                 "Ok\r\n"
                 "x%li\r\ny%li\r\nz%li\r\nm%li\r\n"
                 "Done\r\n",
                 val[X_AXIS_INDEX],
                 val[Y_AXIS_INDEX],
                 val[Z_AXIS_INDEX],
                 val[M_AXIS_INDEX]);
    }
    /* send values of specified axis only*/
    else
    {
        uint8_t is_command_valid = 1;

        uint16_t out_written = snprintf(out_buffer, out_max, "%s", "Ok\r\n") - 1; /** exclude null termination */

        for (uint8_t i = 1; i < argc; i++)
        {
            if (CLI_Get_Argument_Length(argv[i]) == 1)
            {
                char axis = *argv[i];
                int32_t temp = 0;

                switch (axis)
                {
                case 'x':
                case 'y':
                case 'z':
                case 'm':

                    temp = val[Motor_Name_To_Index(axis)];

                    out_written += snprintf(out_buffer + out_written,
                                            out_max - out_written,
                                            "%c%li\r\n",
                                            axis,
                                            temp);

                    /** exclude null termination */
                    out_written--;

                    break;

                default:
                    is_command_valid = 0;
                    break;
                }
            }
            else
            {
                is_command_valid = 0;
            }
        }

        if (is_command_valid)
        {
            snprintf(out_buffer + out_written,
                     out_max - out_written,
                     "Done\r\n");
        }
        else
        {
            Sprint_Error_Helper(argv[0], out_buffer, out_max);
        }
    }
}

/**
 * @}
 */
/* End of Sprint_Helper_Functions */

/**
 * @defgroup Set_Param_Helper_Functions
 * @{
 */

/**
 * @brief find and parse axis in input command
 * @param argc argument count in input string
 * @param argv[] argument list
 * @param axis_found[] mark axis to if found
 * @param axis_val[] axis value
 */
uint8_t Parse_Axis_Helper(uint8_t argc,
                          const char *argv[],
                          uint8_t axis_found[],
                          float axis_val[])
{
    uint8_t is_command_valid = 1;
    uint8_t axis = 0;

    for (uint8_t i = 1; i < argc; i++)
    {
        switch (*argv[i])
        {
        case 'x':
        case 'y':
        case 'z':
        case 'm':

            axis = Motor_Name_To_Index(*argv[i]);
            axis_found[axis] = 1;
            is_command_valid = Validate_Expression(argv[i] + 1);

            if (is_command_valid)
            {
                axis_val[axis] = Evaluate_Expression(argv[i] + 1);
            }

            break;

        default:
            is_command_valid = 0;
            break;
        }
    }

    return is_command_valid;
}

/**
 * @}
 */
/* End of Set_Param_Helper_Functions */

/**
 * @defgroup Callback_Implementaion
 * @{
 */

/**
 * @brief Implementation of 'move' command
 *        @see Parse_Axis_Helper
 *        @see Motor_Move
 *
 *        This function is called by cli when "move" command is received. This command
 *        expect at least one argument x,y,z or m
 *
 *        e.g "move x1000 y-2544 z5445"
 *
 *        then parsed command is
 *
 *        argc    = 4        argument count in received string
 *        argv[0] = "move"   command itself
 *        argv[1] = "x1000"  first argument
 *        argv[2] = "y-2544" second argument
 *        argv[3] = "z5445"  third argument
 *
 *        Command with expressions are also supported
 *
 *        e.g "move x(100+5656) y488/254 z(45*78/(45+484))" parenthesis are not necessary
 *
 *        Since most the L6470 commands callback Implementations had same code structure, repeated
 *        code was removed and put into Parse_Axis_Helper() which is common to most L6470 commands
 * 
 *        "move", "run", "goto" and "home" commands produces motion which may take some time to complete
 *        Since every command is responded with "Ok" and "Done" reply if command is valid. "Done" cannot be
 *        replied immediately for motion commands because they may take some time for completion.
 *        Replying "Done" to these commands are handled by @see Print_Done_Task().
 *
 * @param argc number of argument in input string including command itself.
 * @param argv[] argument list, argv[0] is received command itself.
 * @param out_buffer generated output is written to this buffer. This is reply to input command.
 * @param out_max max number of bytes that can be written to out_buffer.
 * @reval return 0, command is complete. @see Help_Callback() for return value of 1.
 */
static uint8_t Move_Callback(uint8_t argc,
                             const char *argv[],
                             char *out_buffer,
                             uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Move(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        snprintf(out_buffer, out_max, "%s", "Ok\r\n");
        /* this is motion command "Done" will be printed by Print_Done_Task after motion is completed*/
        xSemaphoreGive(L6470_Motion_SEM);
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief Implementation of 'goto' command.
 * @see Move_Callback() for function parameters description.
 * @see Motor_Goto()
 */
static uint8_t Goto_Callback(uint8_t argc,
                             const char *argv[],
                             char *out_buffer,
                             uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Goto(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        snprintf(out_buffer, out_max, "%s", "Ok\r\n");
        /* this is motion command "Done" will be printed by Print_Done_Task after motion is completed*/
        xSemaphoreGive(L6470_Motion_SEM);
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief Implementation of 'run' command.
 * @see Move_Callback() for function parameters description.
 * @see Motor_Run()
 */
static uint8_t Run_Callback(uint8_t argc,
                            const char *argv[],
                            char *out_buffer,
                            uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Run(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        snprintf(out_buffer, out_max, "%s", "Ok\r\n");
        /* this is motion command "Done" will be printed by Print_Done_Task after motion is completed*/
        xSemaphoreGive(L6470_Motion_SEM);
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief Implementation of 'home' command.
 * @see Move_Callback() for function parameters description.
 * @see Motor_Home()
 */
static uint8_t Home_Callback(uint8_t argc,
                             const char *argv[],
                             char *out_buffer,
                             uint16_t out_max)

{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Home(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        snprintf(out_buffer, out_max, "%s", "Ok\r\n");
        /* this is motion command "Done" will be printed by Print_Done_Task after motion is completed*/
        xSemaphoreGive(L6470_Motion_SEM);
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return the current positions of all(specified) axis.
 * @see Move_Callback() for function parameters description.
 */
static uint8_t Get_Pos_Callback(uint8_t argc,
                                const char *argv[],
                                char *out_buffer,
                                uint16_t out_max)
{
    float pos[NO_OF_MOTORS];

    Motor_Get_Position(pos);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, pos);

    return 0; // operation complete do not call again
}

/**
 * @brief change the active lens to specified lens
 * @see Move_Callback() for function parameters description.
 */
static uint8_t Set_Lens_Callback(uint8_t argc,
                                 const char *argv[],
                                 char *out_buffer,
                                 uint16_t out_max)
{
    uint8_t is_command_valid = 1;
    uint8_t current_position = Lens_Get_Current_Position();
    uint8_t next_position = 0;

    if (argc == 2)
    {
        if (strcmp(argv[1], Lens_Get_Lens_At_Position(0)) == 0)
        {
            next_position = 0;
        }
        else if (strcmp(argv[1], Lens_Get_Lens_At_Position(1)) == 0)
        {
            next_position = 1;
        }
        else if (strcmp(argv[1], Lens_Get_Lens_At_Position(2)) == 0)
        {
            next_position = 2;
        }
        else if (strcmp(argv[1], Lens_Get_Lens_At_Position(3)) == 0)
        {
            next_position = 3;
        }
        else
        {
            is_command_valid = 0;
        }
    }
    else
    {
        is_command_valid = 0;
    }

    if (is_command_valid)
    {
        uint8_t dir = L6470_DIR_FWD_ID;
        int32_t steps = (next_position - current_position) * MICRO_STEPS_PER_LENS_CHANGE;
        Lens_Set_Current_Position(next_position);

        if (steps > 0)
        {
            dir = L6470_DIR_FWD_ID;
        }
        else
        {
            steps = steps * (-1);
            dir = L6470_DIR_REV_ID;
        }

        L6470_PrepareMove(M_AXIS_INDEX, dir, steps);

        snprintf(out_buffer, out_max, "%s", "Ok\r\n");
        /* this is motion command "Done" will be printed by Print_Done_Task after motion is completed*/
        xSemaphoreGive(L6470_Motion_SEM);
        L6470_PerformPreparedApplicationCommand();
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }
    return 0; // operation complete do not call again
}

/**
 * @brief scan all lenses connected on microscope turret
 * @see Move_Callback() for function parameters description.
 */
static uint8_t Scan_Lens_Callback(uint8_t argc,
                                  const char *argv[],
                                  char *out_buffer,
                                  uint16_t out_max)
{
    // command with no argument
    if (argc == 1)
    {
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
        Lens_Scan_All();
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return the current active lens on microscope
 * @see Move_Callback() for function parameters description.
 */
static uint8_t Get_Lens_Callback(uint8_t argc,
                                 const char *argv[],
                                 char *out_buffer,
                                 uint16_t out_max)
{
    // command with no argument
    if (argc == 1)
    {
        snprintf(out_buffer,
                 out_max,
                 "Ok\r\n"
                 "%s\r\n"
                 "Done\r\n",
                 Lens_Get_Lens_At_Position(Lens_Get_Current_Position()));
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return all the lenses attached to microscope.
 *        lenses are scanned in homing cycle of microscope, @see L6470_TASK()
 *        or scan can be initiated with 'scanlens' command
 * @see Move_Callback() for function parameters description.
 * @see Lens_Get_Lens_At_Position()
 */
static uint8_t Get_Lens_All_Callback(uint8_t argc,
                                     const char *argv[],
                                     char *out_buffer,
                                     uint16_t out_max)
{
    if (argc == 1)
    {
        snprintf(out_buffer,
                 out_max,
                 "Ok\r\n"
                 "%s\r\n%s\r\n%s\r\n%s\r\n"
                 "Done\r\n",
                 Lens_Get_Lens_At_Position(0),
                 Lens_Get_Lens_At_Position(1),
                 Lens_Get_Lens_At_Position(2),
                 Lens_Get_Lens_At_Position(3));
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief set current running speed of given axis
 * @see Move_Callback() for function parameters description.
 * @see Motor_Set_Speed().
 * @note not used.
 */
static uint8_t Set_Speed_Callback(uint8_t argc,
                                  const char *argv[],
                                  char *out_buffer,
                                  uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_Speed(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return current speed of all(specified) the axis.
 * @see Move_Callback() for function parameters description.
 */
static uint8_t Get_Speed_Callback(uint8_t argc,
                                  const char *argv[],
                                  char *out_buffer,
                                  uint16_t out_max)
{
    float speed[NO_OF_MOTORS];

    Motor_Get_Speed(speed);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, speed);

    return 0; // operation complete do not call again
}

/**
 * @brief set minimum running speed of given axis
 * @see Move_Callback() for function parameters description.
 * @see Motor_Set_Min_Speed()
 */
static uint8_t Set_Min_Speed_Callback(uint8_t argc,
                                      const char *argv[],
                                      char *out_buffer,
                                      uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_Min_Speed(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return minimum speed of all(specified) axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_Min_Speed_Callback(uint8_t argc,
                                      const char *argv[],
                                      char *out_buffer,
                                      uint16_t out_max)
{
    float min_speed[NO_OF_MOTORS];

    Motor_Get_Min_Speed(min_speed);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, min_speed);

    return 0; // operation complete do not call again
}

/**
 * @brief set maximum running speed of given axis
 * @see Move_Callback() for function parameters description.
 * @see Motor_Set_Max_Speed()
 */
static uint8_t Set_Max_Speed_Callback(uint8_t argc,
                                      const char *argv[],
                                      char *out_buffer,
                                      uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_Max_Speed(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return maximum speed of all(specified) axis
 * @see Move_Callback() for function parameters description.
 */
static uint8_t Get_Max_Speed_Callback(uint8_t argc,
                                      const char *argv[],
                                      char *out_buffer,
                                      uint16_t out_max)
{
    float max_speed[NO_OF_MOTORS];

    Motor_Get_Max_Speed(max_speed);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, max_speed);

    return 0; // operation complete do not call again
}

/**
 * @brief set acceleration of given axis
 * @see Move_Callback() for function parameters description.
 * @see Motor_Set_ACC()
 */
static uint8_t Set_ACC_Callback(uint8_t argc,
                                const char *argv[],
                                char *out_buffer,
                                uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_ACC(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return acceleration of all(specified) axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_ACC_Callback(uint8_t argc,
                                const char *argv[],
                                char *out_buffer,
                                uint16_t out_max)
{
    float acc[NO_OF_MOTORS];

    Motor_Get_ACC(acc);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, acc);

    return 0; // operation complete do not call again
}

/**
 * @brief set deceleration of given axis
 * @see Move_Callback() for function parameters description.
 * @see Motor_Set_DEC()
 */
static uint8_t Set_DEC_Callback(uint8_t argc,
                                const char *argv[],
                                char *out_buffer,
                                uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_DEC(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return deceleration of all(specified) axis
 * @see Move_Callback() for function parameters description.
 */
static uint8_t Get_DEC_Callback(uint8_t argc,
                                const char *argv[],
                                char *out_buffer,
                                uint16_t out_max)
{
    float dec[NO_OF_MOTORS];

    Motor_Get_DEC(dec);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, dec);

    return 0; // operation complete do not call again
}

/**
 * @brief set holding torque of given axis
 * @see Move_Callback() for function parameters description.
 * @see Motor_Set_KVAL_Hold()
 */
static uint8_t Set_KVAL_Hold_Callback(uint8_t argc,
                                      const char *argv[],
                                      char *out_buffer,
                                      uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_KVAL_Hold(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return holding torque of all(specified) axis
 * @see Move_Callback() for function parameters description.
 */
static uint8_t Get_KVAL_Hold_Callback(uint8_t argc,
                                      const char *argv[],
                                      char *out_buffer,
                                      uint16_t out_max)
{
    float kval[NO_OF_MOTORS];

    Motor_Get_KVAL_Hold(kval);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, kval);

    return 0; // operation complete do not call again
}

/**
 * @brief set running torque of given axis
 * @see Move_Callback() for function parameters description
 * @see Motor_Set_KVAL_Run()
 */
static uint8_t Set_KVAL_Run_Callback(uint8_t argc,
                                     const char *argv[],
                                     char *out_buffer,
                                     uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_KVAL_Run(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return running torque of all(specified) axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_KVAL_Run_Callback(uint8_t argc,
                                     const char *argv[],
                                     char *out_buffer,
                                     uint16_t out_max)
{
    float kval[NO_OF_MOTORS];

    Motor_Get_KVAL_Run(kval);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, kval);

    return 0; // operation complete do not call again
}

/**
 * @brief set accelerating torque of given axis
 * @see Move_Callback() for function parameters description
 * @see Motor_Set_KVAL_ACC()
 */
static uint8_t Set_KVAL_ACC_Callback(uint8_t argc,
                                     const char *argv[],
                                     char *out_buffer,
                                     uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_KVAL_ACC(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return accelerating torque of all(specified) axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_KVAL_ACC_Callback(uint8_t argc,
                                     const char *argv[],
                                     char *out_buffer,
                                     uint16_t out_max)
{
    float kval[NO_OF_MOTORS];

    Motor_Get_KVAL_ACC(kval);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, kval);

    return 0; // operation complete do not call again
}

/**
 * @brief set decelerating torque of given axis
 * @see Move_Callback() for function parameters description
 * @see Motor_Set_KVAL_DEC()
 */
static uint8_t Set_KVAL_DEC_Callback(uint8_t argc,
                                     const char *argv[],
                                     char *out_buffer,
                                     uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_KVAL_DEC(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return decelerating torque of all(specified) axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_KVAL_DEC_Callback(uint8_t argc,
                                     const char *argv[],
                                     char *out_buffer,
                                     uint16_t out_max)
{
    float kval[NO_OF_MOTORS];

    Motor_Get_KVAL_DEC(kval);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, kval);

    return 0; // operation complete do not call again
}

/**
 * @brief set over current thresold of given axis
 * @see Move_Callback() for function parameters description
 * @see Motor_Set_OCD_TH()
 */
static uint8_t Set_OCD_TH_Callback(uint8_t argc,
                                   const char *argv[],
                                   char *out_buffer,
                                   uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_OCD_TH(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return over current threshold of given axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_OCD_TH_Callback(uint8_t argc,
                                   const char *argv[],
                                   char *out_buffer,
                                   uint16_t out_max)
{
    float ocd_th[NO_OF_MOTORS];

    Motor_Get_OCD_TH(ocd_th);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, ocd_th);

    return 0; // operation complete do not call again
}

/**
 * @brief set stall threshold of given axis
 * @see Move_Callback() for function parameters description
 * @see Motor_Set_Stall_TH()
 */
static uint8_t Set_Stall_TH_Callback(uint8_t argc,
                                     const char *argv[],
                                     char *out_buffer,
                                     uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_Stall_TH(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return stall threshold of all(specified) axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_Stall_TH_Callback(uint8_t argc,
                                     const char *argv[],
                                     char *out_buffer,
                                     uint16_t out_max)
{
    float stall_th[NO_OF_MOTORS];

    Motor_Get_Stall_TH(stall_th);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, stall_th);

    return 0; // operation complete do not call again
}

/**
 * @brief set microsteps resolution of given axis
 * @see Move_Callback() for function parameters description
 * @see Motor_Set_Microsteps()
 */
static uint8_t Set_Microsteps_Callback(uint8_t argc,
                                       const char *argv[],
                                       char *out_buffer,
                                       uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_Microsteps(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return microsteps resolution of all(specified) axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_Microsteps_Callback(uint8_t argc,
                                       const char *argv[],
                                       char *out_buffer,
                                       uint16_t out_max)
{
    float microsteps[NO_OF_MOTORS];

    Motor_Get_Microsteps(microsteps);

    Sprint_Reply_Float_Helper(argc, argv, out_buffer, out_max, microsteps);

    return 0; // operation complete do not call again
}

/**
 * @brief set configuration register of given axis
 * @see Move_Callback() for function parameters description
 * @see Motor_Set_Config()
 */
static uint8_t Set_Config_Callback(uint8_t argc,
                                   const char *argv[],
                                   char *out_buffer,
                                   uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Set_Config(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return configuration register of all(specified) axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_Config_Callback(uint8_t argc,
                                   const char *argv[],
                                   char *out_buffer,
                                   uint16_t out_max)
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 config registers */
    Motor_Read_Register(L6470_CONFIG_ID, reg);

    Sprint_Reply_Int_Helper(argc, argv, out_buffer, out_max, (int32_t *)reg);

    return 0; // operation complete do not call again
}

/**
 * @brief put given axis in high impedance mode
 * @see Move_Callback() for function parameters description
 * @see Motor_Release()
 */
static uint8_t Release_Callback(uint8_t argc,
                                const char *argv[],
                                char *out_buffer,
                                uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Motor_Release(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief set brightness of illumination LED from 0 to 100%
 * @param argc
 * @param argv[]
 * @param out_buffer
 * @param out_max
 * @retval 0
 */
static uint8_t Set_Brightness_Callback(uint8_t argc,
                                       const char *argv[],
                                       char *out_buffer,
                                       uint16_t out_max)
{
    uint8_t is_command_valid = 0;
    float pwm_value = 0;

    for (uint8_t i = 1; i < argc; i++)
    {
        /* reset is_command_valid for every iteration */
        is_command_valid = 0;

        if (Validate_Expression(argv[i] + 4))
        {
            pwm_value = Evaluate_Expression(argv[i] + 4);

            if (pwm_value > 0 && pwm_value < WLED_PWM_MAX + 1)
            {
                if (strncmp(argv[i], "pled", 4) == 0)
                {
                    is_command_valid = 1;
                    WLED_SET_BRIGHTNESS((uint32_t)pwm_value);
                }
                else if (strncmp(argv[i], "sled", 4) == 0)
                {
                    is_command_valid = 1;
                    //WLED_SET_BRIGHTNESS((uint32_t)pwm_value);
                }
            }
        }
    }

    if (is_command_valid)
    {
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief set joystick mode of given axis
 * @see Move_Callback() for function parameters description
 * @see Joystick_Set_Mode()
 */
static uint8_t Set_Joystick_Mode_Callback(uint8_t argc,
                                          const char *argv[],
                                          char *out_buffer,
                                          uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Joystick_Set_Mode(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return joystick mode of all(specified) axis
 *        not applicable for m axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_Joystick_Mode_Callback(uint8_t argc,
                                          const char *argv[],
                                          char *out_buffer,
                                          uint16_t out_max)
{
    int32_t reg[NO_OF_MOTORS];

    /* read joystick mode of all axis*/
    Joystick_Get_Mode((uint8_t *)reg);

    Sprint_Reply_Int_Helper(argc, argv, out_buffer, out_max, reg);

    return 0; // operation complete do not call again
}

/**
 * @brief set joystick multiplier
 * @see Move_Callback() for function parameters description
 * @see Joystick_Set_Multiplier()
 */
static uint8_t Set_Joystick_Multiplier_Callback(uint8_t argc,
                                                const char *argv[],
                                                char *out_buffer,
                                                uint16_t out_max)
{
    uint8_t is_cmd_ok = 0;

    /* at least one axis must be specified */
    if (argc > 1)
    {
        uint8_t axis_found[NO_OF_MOTORS] = {0, 0, 0, 0};
        float axis_val[NO_OF_MOTORS] = {0, 0, 0, 0};

        if (Parse_Axis_Helper(argc, argv, axis_found, axis_val))
        {
            is_cmd_ok = Joystick_Set_Multiplier(axis_found, axis_val);
        }
    }

    if (is_cmd_ok)
    {
        /* not motion command, directly print "Done" */
        snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief return joystick mode of all(specified) axis
 *        not applicable for m axis
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_Joystick_Multiplier_Callback(uint8_t argc,
                                                const char *argv[],
                                                char *out_buffer,
                                                uint16_t out_max)
{
    int32_t reg[NO_OF_MOTORS];

    /* read joystick mode of all axis*/
    Joystick_Get_Multiplier((uint16_t *)reg);

    Sprint_Reply_Int_Helper(argc, argv, out_buffer, out_max, reg);

    return 0; // operation complete do not call again
}

/**
 * @brief return current firmware version
 * @see Move_Callback() for function parameters description
 */
static uint8_t Get_Version_Callback(uint8_t argc,
                                    const char *argv[],
                                    char *out_buffer,
                                    uint16_t out_max)
{
    if (argc == 1)
    {
        snprintf(out_buffer,
                 out_max,
                 "Ok\r\n"
                 "%s\r\n"
                 "Done\r\n",
                 Firmware_Version);
    }
    else
    {
        Sprint_Error_Helper(argv[0], out_buffer, out_max);
    }

    return 0; // operation complete do not call again
}

/**
 * @brief jump to bootloader for firmware update
 * @see Move_Callback() for function parameters description
 */
static uint8_t Update_Callback(uint8_t argc,
                               const char *argv[],
                               char *out_buffer,
                               uint16_t out_max)
{
    uint8_t xreturn = 0;
    static uint8_t ok_done_sent = 0;

    if (!ok_done_sent)
    {
        if (argc == 1)
        {
            /* call again after sending status ack, to jump to bootloader*/
            xreturn = 1;
            snprintf(out_buffer, out_max, "%s", "Ok\r\nDone\r\n");
            ok_done_sent = 1;
        }
        else
        {
            Sprint_Error_Helper(argv[0], out_buffer, out_max);
        }
    }
    else
    {
        ok_done_sent = 0;

        /** enable backup register access */
        __HAL_RCC_PWR_CLK_ENABLE();
        HAL_PWR_EnableBkUpAccess();
        __HAL_RCC_BKP_CLK_ENABLE();

        /** write magic number in backup register, so bootloader know it needs to update firmware */
        BKP->DR1 = 0xA5;

        /* reset system, hand over cpu to bootloader*/
        HAL_NVIC_SystemReset();
    }

    return xreturn;
}

/**
 * @}
 */
/* End of Callback_Implementaion */

/**
 * @brief add all the commands to CLI list
 */
void CLI_Add_Microscope_Commands()
{
    CLI_Add_Command(&Move_Definition);
    CLI_Add_Command(&Goto_Definition);
    CLI_Add_Command(&Run_Definition);
    CLI_Add_Command(&Home_Definition);

    CLI_Add_Command(&Get_Pos_Definition);

    CLI_Add_Command(&Scan_Lens_Definition);
    CLI_Add_Command(&Set_Lens_Definition);
    CLI_Add_Command(&Get_Lens_Definition);
    CLI_Add_Command(&Get_Lens_All_Definition);

    CLI_Add_Command(&Set_Speed_Definition);
    CLI_Add_Command(&Get_Speed_Definition);

    CLI_Add_Command(&Set_Min_Speed_Definition);
    CLI_Add_Command(&Get_Min_Speed_Definition);

    CLI_Add_Command(&Set_Max_Speed_Definition);
    CLI_Add_Command(&Get_Max_Speed_Definition);

    CLI_Add_Command(&Set_ACC_Definition);
    CLI_Add_Command(&Get_ACC_Definition);

    CLI_Add_Command(&Set_DEC_Definition);
    CLI_Add_Command(&Get_DEC_Definition);

    CLI_Add_Command(&Set_KVAL_Hold_Definition);
    CLI_Add_Command(&Get_KVAL_Hold_Definition);

    CLI_Add_Command(&Set_KVAL_Run_Definition);
    CLI_Add_Command(&Get_KVAL_Run_Definition);

    CLI_Add_Command(&Set_KVAL_ACC_Definition);
    CLI_Add_Command(&Get_KVAL_ACC_Definition);

    CLI_Add_Command(&Set_KVAL_DEC_Definition);
    CLI_Add_Command(&Get_KVAL_DEC_Definition);

    CLI_Add_Command(&Set_OCD_TH_Definition);
    CLI_Add_Command(&Get_OCD_TH_Definition);

    CLI_Add_Command(&Set_Stall_TH_Definition);
    CLI_Add_Command(&Get_Stall_TH_Definition);

    CLI_Add_Command(&Set_Microsteps_Definition);
    CLI_Add_Command(&Get_Microsteps_Definition);

    CLI_Add_Command(&Set_Config_Definition);
    CLI_Add_Command(&Get_Config_Definition);

    CLI_Add_Command(&Set_Joystick_Mode_Definition);
    CLI_Add_Command(&Get_Joystick_Mode_Definition);

    CLI_Add_Command(&Set_Joystick_Multiplier_Definition);
    CLI_Add_Command(&Get_Joystick_Multiplier_Definition);

    CLI_Add_Command(&Set_Brightness_Definition);

    CLI_Add_Command(&Release_Definition);
    CLI_Add_Command(&Update_Definition);

    CLI_Add_Command(&Get_Version_Definition);

    Print_Done_Task_Handle = xTaskCreateStatic(Print_Done_Task,
                                               "Print_Done_Task",
                                               PRINT_DONE_TASK_STACK_SIZE,
                                               NULL,
                                               PRINT_DONE_TASK_PRIORITY,
                                               Print_Done_Task_Stack,
                                               &Print_Done_Task_TCB);

    Ping_Timer_Handle = xTimerCreateStatic("Ping_Timer",
                                           3000,
                                           pdTRUE,
                                           (void *)0,
                                           Ping_Timer_Callback,
                                           &Ping_Timer_TCB);

    xTimerStart(Ping_Timer_Handle, 1000);
}

/**
 * @brief send "Done" string asynchronously when motion command completes
 * @param argument FreeRTOS input argument
 */
static void Print_Done_Task(void *argument)
{
    while (1)
    {
        /** wait for idle signal, semaphore is given from L6470 ISR @see l6470_thread.c */
        xSemaphoreTake(L6470_Idle_SEM, portMAX_DELAY);

        if (xSemaphoreTake(L6470_Motion_SEM, 0))
        {
            /** gaurd uart */
            xSemaphoreTake(CLI_UART_Access_Mutex, portMAX_DELAY);

            CLI_UART_Send_String("Done\r\n");

            /** release uart */
            xSemaphoreGive(CLI_UART_Access_Mutex);
        }
    }
}

/**
 * @brief send Microscope status asynchronously every 3 second (alive signal)
 * @param argument FreeRTOS Timer handle
 */
static void Ping_Timer_Callback(TimerHandle_t xTimer)
{
    char uart_stream[30];

    uint16_t out_written = snprintf(uart_stream, sizeof(uart_stream), "%s", "ping\r\n") - 1;

    if (Z_Axis_Min_Speed > 50)
    {
        snprintf(uart_stream + out_written, sizeof(uart_stream) - out_written, "%s", "coarse\r\n");
    }
    else
    {
        snprintf(uart_stream + out_written, sizeof(uart_stream) - out_written, "%s", "fine\r\n");
    }

    if (Ping_Enable_Flag)
    {
        /** gaurd uart if available*/
        if (xSemaphoreTake(CLI_UART_Access_Mutex, 0))
        {
            CLI_UART_Send_String(uart_stream);

            /** release uart */
            xSemaphoreGive(CLI_UART_Access_Mutex);
        }
    }
}

/**
 * @}
 */
/* End of CLI_Commands */
