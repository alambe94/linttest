/**
 * @file motor_config.c
 * @brief configure motor parameters
 * @author medprime (www.medprimetech.com)
 * @version 0.0.0
 */

#include "motor_config.h"
#include "microscope_config.h"
#include "l6470_api.h"

/**
 * @addtogroup Motor_Control
 * @{
 */

/**
 * @defgroup Motor_Config_Gloabals
 * @{
 */

/** full steps in 1 micron */
float Motor_Full_Steps_Per_Micron[NO_OF_MOTORS];

/** micro steps in 1 micron */
float Motor_Micro_Steps_Per_Micron[NO_OF_MOTORS];

/** axis travel limit in micro meter */
float Motor_MAX_Limit[NO_OF_MOTORS];

/**
 * @}
 */
/* End of Motor_Config_Gloabals */

/**
 * @defgroup Motor_Config_Functions
 * @{
 */

/**
 * @brief configure all the motors with default parameters
 */
void Motor_Config()
{
    /** set dafault full steps/micron ratio */
    Motor_Full_Steps_Per_Micron[X_AXIS_INDEX] = FULL_STEPS_PER_MICRON_X;
    Motor_Full_Steps_Per_Micron[Y_AXIS_INDEX] = FULL_STEPS_PER_MICRON_Y;
    Motor_Full_Steps_Per_Micron[Z_AXIS_INDEX] = FULL_STEPS_PER_MICRON_Z;
    Motor_Full_Steps_Per_Micron[M_AXIS_INDEX] = FULL_STEPS_PER_MICRON_M;

    /** set dafault micro steps/micron ratio */
    Motor_Micro_Steps_Per_Micron[X_AXIS_INDEX] = MICRO_STEPS_PER_MICRON_X;
    Motor_Micro_Steps_Per_Micron[Y_AXIS_INDEX] = MICRO_STEPS_PER_MICRON_Y;
    Motor_Micro_Steps_Per_Micron[Z_AXIS_INDEX] = MICRO_STEPS_PER_MICRON_Z;
    Motor_Micro_Steps_Per_Micron[M_AXIS_INDEX] = MICRO_STEPS_PER_MICRON_M;

    Motor_MAX_Limit[X_AXIS_INDEX] = MAX_LIMIT_X;
    Motor_MAX_Limit[Y_AXIS_INDEX] = MAX_LIMIT_Y;
    Motor_MAX_Limit[Z_AXIS_INDEX] = MAX_LIMIT_Z;
    Motor_MAX_Limit[M_AXIS_INDEX] = MAX_LIMIT_M;

    L6470_ENABLE();

    L6470_ResetDevice(X_AXIS_INDEX);
    L6470_GetStatus(X_AXIS_INDEX);

    L6470_ResetDevice(Y_AXIS_INDEX);
    L6470_GetStatus(Y_AXIS_INDEX);

    L6470_ResetDevice(Z_AXIS_INDEX);
    L6470_GetStatus(Z_AXIS_INDEX);

    L6470_ResetDevice(M_AXIS_INDEX);
    L6470_GetStatus(M_AXIS_INDEX);

    /** X axis patrameters */
    float speed_x = 2000;               // micron/sec
    float acc_x = 1000;                 // micron/sec/sec
    float dec_x = 500;                  // micron/sec/sec
    float min_speed_x = 100;            // micron/sec
    float max_speed_x = 14000;           // micron/sec
    float fs_speed_x = 30000;           // micron/sec if fs_speed > max_speed, stepper always run in microstepping mode
    float kval_hold_x = 3.5;            // volt
    float kval_run_x = 3.5;             // volt
    float kval_acc_x = 3.5;             // volt
    float kval_dec_x = 3.5;             // volt
    float ocd_th_x = 2200.0;            // ma
    float stall_th_x = 2000.0;          // ma
    float step_sel_x = MICROSTEP_1_128; // micro steps per full step
    uint32_t alarm_en_x = 0xFF;         // alarm conditions enable
    uint32_t config_reg_x = 0x2E88;     // ic configuration

    /** Y axis patrameters */
    float speed_y = 2000;               // micron/sec
    float acc_y = 1000;                 // micron/sec/sec
    float dec_y = 500;                  // micron/sec/sec
    float min_speed_y = 100;            // micron/sec
    float max_speed_y = 14000;           // micron/sec
    float fs_speed_y = 30000;           // micron/sec
    float kval_hold_y = 3.5;            // volt
    float kval_run_y = 3.5;             // volt
    float kval_acc_y = 3.5;             // volt
    float kval_dec_y = 3.5;             // volt
    float ocd_th_y = 2200.0;            // ma
    float stall_th_y = 2000.0;          // ma
    float step_sel_y = MICROSTEP_1_128; // micro steps per full step
    uint32_t alarm_en_y = 0xFF;         // alarm conditions enable
    uint32_t config_reg_y = 0x2E88;     // ic configuration

    /** Z axis patrameters */
    float speed_z = 2000;               // micron/sec
    float acc_z = 1000;                 // micron/sec/sec
    float dec_z = 500;                  // micron/sec/sec
    float min_speed_z = 1000;           // micron/sec
    float max_speed_z = 14000;           // micron/sec
    float fs_speed_z = 30000;           // micron/sec
    float kval_hold_z = 3.5;            // volt
    float kval_run_z = 3.5;             // volt
    float kval_acc_z = 3.5;             // volt
    float kval_dec_z = 3.5;             // volt
    float ocd_th_z = 2200.0;            // ma
    float stall_th_z = 2000.0;          // ma
    float step_sel_z = MICROSTEP_1_128; // micro steps per full step
    uint32_t alarm_en_z = 0xFF;         // alarm conditions enable
    uint32_t config_reg_z = 0x2E88;     // ic configuration

    /** M axis patrameters */
    float speed_m = 2000;               // micron/sec
    float acc_m = 1000;                 // micron/sec/sec
    float dec_m = 500;                  // micron/sec/sec
    float min_speed_m = 500;            // micron/sec
    float max_speed_m = 14000;           // micron/sec
    float fs_speed_m = 30000;           // micron/sec
    float kval_hold_m = 3.5;            // volt
    float kval_run_m = 5.5;             // volt
    float kval_acc_m = 5.5;             // volt
    float kval_dec_m = 5.5;             // volt
    float ocd_th_m = 2200.0;            // ma
    float stall_th_m = 2000.0;          // ma
    float step_sel_m = MICROSTEP_1_128; // micro steps per full step
    uint32_t alarm_en_m = 0xFF;         // alarm conditions enable
    uint32_t config_reg_m = 0x2E88;     // ic configuration

    Axis_Set_Speed(X_AXIS_INDEX, speed_x);
    Axis_Set_Speed(Y_AXIS_INDEX, speed_y);
    Axis_Set_Speed(Z_AXIS_INDEX, speed_z);
    Axis_Set_Speed(M_AXIS_INDEX, speed_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_Min_Speed(X_AXIS_INDEX, min_speed_x);
    Axis_Set_Min_Speed(Y_AXIS_INDEX, min_speed_y);
    Axis_Set_Min_Speed(Z_AXIS_INDEX, min_speed_z);
    Axis_Set_Min_Speed(M_AXIS_INDEX, min_speed_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_Max_Speed(X_AXIS_INDEX, max_speed_x);
    Axis_Set_Max_Speed(Y_AXIS_INDEX, max_speed_y);
    Axis_Set_Max_Speed(Z_AXIS_INDEX, max_speed_z);
    Axis_Set_Max_Speed(M_AXIS_INDEX, max_speed_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_FS_Speed(X_AXIS_INDEX, fs_speed_x);
    Axis_Set_FS_Speed(Y_AXIS_INDEX, fs_speed_y);
    Axis_Set_FS_Speed(Z_AXIS_INDEX, fs_speed_z);
    Axis_Set_FS_Speed(M_AXIS_INDEX, fs_speed_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_ACC(X_AXIS_INDEX, acc_x);
    Axis_Set_ACC(Y_AXIS_INDEX, acc_y);
    Axis_Set_ACC(Z_AXIS_INDEX, acc_z);
    Axis_Set_ACC(M_AXIS_INDEX, acc_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_DEC(X_AXIS_INDEX, dec_x);
    Axis_Set_DEC(Y_AXIS_INDEX, dec_y);
    Axis_Set_DEC(Z_AXIS_INDEX, dec_z);
    Axis_Set_DEC(M_AXIS_INDEX, dec_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_KVAL_Hold(X_AXIS_INDEX, kval_hold_x);
    Axis_Set_KVAL_Hold(Y_AXIS_INDEX, kval_hold_y);
    Axis_Set_KVAL_Hold(Z_AXIS_INDEX, kval_hold_z);
    Axis_Set_KVAL_Hold(M_AXIS_INDEX, kval_hold_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_KVAL_Run(X_AXIS_INDEX, kval_run_x);
    Axis_Set_KVAL_Run(Y_AXIS_INDEX, kval_run_y);
    Axis_Set_KVAL_Run(Z_AXIS_INDEX, kval_run_z);
    Axis_Set_KVAL_Run(M_AXIS_INDEX, kval_run_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_KVAL_ACC(X_AXIS_INDEX, kval_acc_x);
    Axis_Set_KVAL_ACC(Y_AXIS_INDEX, kval_acc_y);
    Axis_Set_KVAL_ACC(Z_AXIS_INDEX, kval_acc_z);
    Axis_Set_KVAL_ACC(M_AXIS_INDEX, kval_acc_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_KVAL_DEC(X_AXIS_INDEX, kval_dec_x);
    Axis_Set_KVAL_DEC(Y_AXIS_INDEX, kval_dec_y);
    Axis_Set_KVAL_DEC(Z_AXIS_INDEX, kval_dec_z);
    Axis_Set_KVAL_DEC(M_AXIS_INDEX, kval_dec_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_OCD_TH(X_AXIS_INDEX, ocd_th_x);
    Axis_Set_OCD_TH(Y_AXIS_INDEX, ocd_th_y);
    Axis_Set_OCD_TH(Z_AXIS_INDEX, ocd_th_z);
    Axis_Set_OCD_TH(M_AXIS_INDEX, ocd_th_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_Stall_TH(X_AXIS_INDEX, stall_th_x);
    Axis_Set_Stall_TH(Y_AXIS_INDEX, stall_th_y);
    Axis_Set_Stall_TH(Z_AXIS_INDEX, stall_th_z);
    Axis_Set_Stall_TH(M_AXIS_INDEX, stall_th_m);
    L6470_PerformPreparedApplicationCommand();

    L6470_PrepareSetParam(X_AXIS_INDEX, L6470_ALARM_EN_ID, alarm_en_x);
    L6470_PrepareSetParam(Y_AXIS_INDEX, L6470_ALARM_EN_ID, alarm_en_y);
    L6470_PrepareSetParam(Z_AXIS_INDEX, L6470_ALARM_EN_ID, alarm_en_z);
    L6470_PrepareSetParam(M_AXIS_INDEX, L6470_ALARM_EN_ID, alarm_en_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_Microstep(X_AXIS_INDEX, step_sel_x);
    Axis_Set_Microstep(Y_AXIS_INDEX, step_sel_y);
    Axis_Set_Microstep(Z_AXIS_INDEX, step_sel_z);
    Axis_Set_Microstep(M_AXIS_INDEX, step_sel_m);
    L6470_PerformPreparedApplicationCommand();

    Axis_Set_Config(X_AXIS_INDEX, config_reg_x);
    Axis_Set_Config(Y_AXIS_INDEX, config_reg_y);
    Axis_Set_Config(Z_AXIS_INDEX, config_reg_z);
    Axis_Set_Config(M_AXIS_INDEX, config_reg_m);
    L6470_PerformPreparedApplicationCommand();
}

/**
 * @brief return no of maximum micrometer a motor can move
 * @param axis_index selected stepper
 * @retval  maximum steps
 */
float Motor_Get_MAX_Limit(uint8_t axis_index)
{
    return Motor_MAX_Limit[axis_index];
}

/**
 * @brief return current position of a motor, read steps from L6470, convert it into micrometer
 * @param axis_index selected stepper
 * @retval  current steps
 * @note must acquire l6470 mutex before calling this funtion
 */
float Motor_Get_Current_Position(uint8_t axis_index)
{
    return AbsPos_2_Position(L6470_GetParam(axis_index, L6470_ABS_POS_ID)) / Motor_Get_Micro_Steps_Per_Micron(axis_index);
}

/**
 * @brief return microstep resolution of motor
 * @param axis_index selected stepper
 * @retval  current steps
 * @note must acquire l6470 mutex before calling this function
 */
uint8_t Motor_Get_Micro_Steps_Per_Full_Step(uint8_t axis_index)
{
    return AbsPos_2_Position(L6470_GetParam(axis_index, L6470_STEP_MODE_ID));
}

/**
 * @brief set no of full steps in 1 micrometer
 * @param axis_index selected stepper
 */
void Motor_Set_Full_Steps_Per_Micron(uint8_t axis_index, float ratio)
{
    Motor_Full_Steps_Per_Micron[axis_index] = ratio;
}

/**
 * @brief return no of full steps in 1 micrometer
 * @param axis_index selected stepper
 * @retval  full steps
 * @note used for conversion to and from speed, acc, dec
 */
float Motor_Get_Full_Steps_Per_Micron(uint8_t axis_index)
{
    return Motor_Full_Steps_Per_Micron[axis_index];
}

/**
 * @brief set no of micro steps in 1 micrometer
 * @param axis_index selected stepper
 */
void Motor_Set_Micro_Steps_Per_Micron(uint8_t axis_index, float ratio)
{
    Motor_Micro_Steps_Per_Micron[axis_index] = ratio;
}

/**
 * @brief return no of micro steps in 1 micrometer
 * @param axis_index selected stepper
 * @retval  maximum steps
 * @note used for conversion to and from move, goto, position
 */
float Motor_Get_Micro_Steps_Per_Micron(uint8_t axis_index)
{
    return Motor_Micro_Steps_Per_Micron[axis_index];
}

/**
 * @brief read given register of L6470 from all drivers in chain
 * @param reg_id register to read
 * @note must acquire l6470 mutex before calling this funtion
 */
void Motor_Read_Register(eL6470_RegId_t reg_id, int32_t reg[])
{
    uint8_t reg_len = L6470_Register[reg_id].LengthByte;

    L6470_PrepareGetParam(X_AXIS_INDEX, reg_id);
    L6470_PrepareGetParam(Y_AXIS_INDEX, reg_id);
    L6470_PrepareGetParam(Z_AXIS_INDEX, reg_id);
    L6470_PrepareGetParam(M_AXIS_INDEX, reg_id);

    L6470_PerformPreparedApplicationCommand();

    reg[X_AXIS_INDEX] = L6470_ExtractReturnedData(X_AXIS_INDEX,
                                                  (uint8_t *)L6470_DaisyChainSpiRxStruct,
                                                  reg_len);

    reg[Y_AXIS_INDEX] = L6470_ExtractReturnedData(Y_AXIS_INDEX,
                                                  (uint8_t *)L6470_DaisyChainSpiRxStruct,
                                                  reg_len);

    reg[Z_AXIS_INDEX] = L6470_ExtractReturnedData(Z_AXIS_INDEX,
                                                  (uint8_t *)L6470_DaisyChainSpiRxStruct,
                                                  reg_len);

    reg[M_AXIS_INDEX] = L6470_ExtractReturnedData(M_AXIS_INDEX,
                                                  (uint8_t *)L6470_DaisyChainSpiRxStruct,
                                                  reg_len);
}

/**
 * @brief return index of motor
 * @param name
 * @retval axis
 */
uint8_t Motor_Name_To_Index(char name)
{
    uint8_t temp = 0xFF;
    switch (name)
    {
    case 'x':
        temp = X_AXIS_INDEX;
        break;
    case 'y':
        temp = Y_AXIS_INDEX;
        break;
    case 'z':
        temp = Z_AXIS_INDEX;
        break;
    case 'm':
        temp = M_AXIS_INDEX;
        break;
    }
    return temp;
}

/**
 * @}
 */
/* End of Motor_Config_Functions */

/**
 * @}
 */
/* End of Motor_Control */
