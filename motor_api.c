/**
 * @file motor_api.c
 * @brief configure motor parameters
 * @author medprime (www.medprimetech.com)
 * @version 0.0.0
 */

/** system includes */
#include <math.h>

/** st includes */
#include "L6470.h"

/** app includes */
#include "motor_api.h"
#include "microscope_config.h"

/**
 * @addtogroup Motor_Control
 * @{
 */

/**
 * @defgroup Motor_API_Globals
 * @{
 */

/** full steps in 1 micron */
float Motor_Full_Steps_Per_Micron[NO_OF_MOTORS];

/** micro steps in 1 micron */
float Motor_Micro_Steps_Per_Micron[NO_OF_MOTORS];

/** axis travel limit in micro meter */
float Motor_Max_Limit[NO_OF_MOTORS];

/**
 * @}
 */
/* End of Motor_API_Gloabals */

/**
 * @defgroup Motor_API_Functions
 * @{
 */

/**
 * @brief configure all the motors with default parameters
 */
void Motor_Config()
{
    /** set dafault full steps/micron ratio */
    Motor_Full_Steps_Per_Micron[X_AXIS_INDEX] = FULL_STEPS_PER_MICRON_X;
    Motor_Full_Steps_Per_Micron[Y_AXIS_INDEX] = FULL_STEPS_PER_MICRON_Y;
    Motor_Full_Steps_Per_Micron[Z_AXIS_INDEX] = FULL_STEPS_PER_MICRON_Z;
    Motor_Full_Steps_Per_Micron[M_AXIS_INDEX] = FULL_STEPS_PER_MICRON_M;

    /** set dafault micro steps/micron ratio */
    Motor_Micro_Steps_Per_Micron[X_AXIS_INDEX] = MICRO_STEPS_PER_MICRON_X;
    Motor_Micro_Steps_Per_Micron[Y_AXIS_INDEX] = MICRO_STEPS_PER_MICRON_Y;
    Motor_Micro_Steps_Per_Micron[Z_AXIS_INDEX] = MICRO_STEPS_PER_MICRON_Z;
    Motor_Micro_Steps_Per_Micron[M_AXIS_INDEX] = MICRO_STEPS_PER_MICRON_M;

    Motor_Max_Limit[X_AXIS_INDEX] = MAX_LIMIT_X;
    Motor_Max_Limit[Y_AXIS_INDEX] = MAX_LIMIT_Y;
    Motor_Max_Limit[Z_AXIS_INDEX] = MAX_LIMIT_Z;
    Motor_Max_Limit[M_AXIS_INDEX] = MAX_LIMIT_M;

    L6470_ENABLE();

    L6470_ResetDevice(X_AXIS_INDEX);
    L6470_GetStatus(X_AXIS_INDEX);

    L6470_ResetDevice(Y_AXIS_INDEX);
    L6470_GetStatus(Y_AXIS_INDEX);

    L6470_ResetDevice(Z_AXIS_INDEX);
    L6470_GetStatus(Z_AXIS_INDEX);

    L6470_ResetDevice(M_AXIS_INDEX);
    L6470_GetStatus(M_AXIS_INDEX);

    float speed[NO_OF_MOTORS];         // micron/sec
    float acc[NO_OF_MOTORS];           // micron/sec/sec
    float dec[NO_OF_MOTORS];           // micron/sec/sec
    float min_speed[NO_OF_MOTORS];     // micron/sec
    float max_speed[NO_OF_MOTORS];     // micron/sec
    float fs_speed[NO_OF_MOTORS];      // micron/sec if fs_speed > max_speed, stepper always run in microstepping mode
    float kval_hold[NO_OF_MOTORS];     // volt
    float kval_run[NO_OF_MOTORS];      // volt
    float kval_acc[NO_OF_MOTORS];      // volt
    float kval_dec[NO_OF_MOTORS];      // volt
    float ocd_th[NO_OF_MOTORS];        // ma
    float stall_th[NO_OF_MOTORS];      // ma
    float step_sel[NO_OF_MOTORS];      // micro steps per full step
    float alarm_en[NO_OF_MOTORS];   // alarm conditions enable
    float config_reg[NO_OF_MOTORS]; // ic configuration

    /** X axis patrameters */
    speed[X_AXIS_INDEX] = 2000;               // micron/sec
    acc[X_AXIS_INDEX] = 1000;                 // micron/sec/sec
    dec[X_AXIS_INDEX] = 500;                  // micron/sec/sec
    min_speed[X_AXIS_INDEX] = 100;            // micron/sec
    max_speed[X_AXIS_INDEX] = 14000;          // micron/sec
    fs_speed[X_AXIS_INDEX] = 30000;           // micron/sec if fs_speed > max_speed, stepper always run in microstepping mode
    kval_hold[X_AXIS_INDEX] = 3.5;            // volt
    kval_run[X_AXIS_INDEX] = 3.5;             // volt
    kval_acc[X_AXIS_INDEX] = 3.5;             // volt
    kval_dec[X_AXIS_INDEX] = 3.5;             // volt
    ocd_th[X_AXIS_INDEX] = 2200.0;            // ma
    stall_th[X_AXIS_INDEX] = 2000.0;          // ma
    step_sel[X_AXIS_INDEX] = MICROSTEP_1_128; // micro steps per full step
    alarm_en[X_AXIS_INDEX] = 0xFF;            // alarm conditions enable
    config_reg[X_AXIS_INDEX] = 0x2E88;        // ic configuration

    /** Y axis patrameters */
    speed[Y_AXIS_INDEX] = 2000;               // micron/sec
    acc[Y_AXIS_INDEX] = 1000;                 // micron/sec/sec
    dec[Y_AXIS_INDEX] = 500;                  // micron/sec/sec
    min_speed[Y_AXIS_INDEX] = 100;            // micron/sec
    max_speed[Y_AXIS_INDEX] = 14000;          // micron/sec
    fs_speed[Y_AXIS_INDEX] = 30000;           // micron/sec
    kval_hold[Y_AXIS_INDEX] = 3.5;            // volt
    kval_run[Y_AXIS_INDEX] = 3.5;             // volt
    kval_acc[Y_AXIS_INDEX] = 3.5;             // volt
    kval_dec[Y_AXIS_INDEX] = 3.5;             // volt
    ocd_th[Y_AXIS_INDEX] = 2200.0;            // ma
    stall_th[Y_AXIS_INDEX] = 2000.0;          // ma
    step_sel[Y_AXIS_INDEX] = MICROSTEP_1_128; // micro steps per full step
    alarm_en[Y_AXIS_INDEX] = 0xFF;            // alarm conditions enable
    config_reg[Y_AXIS_INDEX] = 0x2E88;        // ic configuration

    /** Z axis patrameters */
    speed[Z_AXIS_INDEX] = 2000;               // micron/sec
    acc[Z_AXIS_INDEX] = 1000;                 // micron/sec/sec
    dec[Z_AXIS_INDEX] = 500;                  // micron/sec/sec
    min_speed[Z_AXIS_INDEX] = 1000;           // micron/sec
    max_speed[Z_AXIS_INDEX] = 14000;          // micron/sec
    fs_speed[Z_AXIS_INDEX] = 30000;           // micron/sec
    kval_hold[Z_AXIS_INDEX] = 3.5;            // volt
    kval_run[Z_AXIS_INDEX] = 3.5;             // volt
    kval_acc[Z_AXIS_INDEX] = 3.5;             // volt
    kval_dec[Z_AXIS_INDEX] = 3.5;             // volt
    ocd_th[Z_AXIS_INDEX] = 2200.0;            // ma
    stall_th[Z_AXIS_INDEX] = 2000.0;          // ma
    step_sel[Z_AXIS_INDEX] = MICROSTEP_1_128; // micro steps per full step
    alarm_en[Z_AXIS_INDEX] = 0xFF;            // alarm conditions enable
    config_reg[Z_AXIS_INDEX] = 0x2E88;        // ic configuration

    /** M axis patrameters */
    speed[M_AXIS_INDEX] = 2000;               // micron/sec
    acc[M_AXIS_INDEX] = 1000;                 // micron/sec/sec
    dec[M_AXIS_INDEX] = 500;                  // micron/sec/sec
    min_speed[M_AXIS_INDEX] = 500;            // micron/sec
    max_speed[M_AXIS_INDEX] = 14000;          // micron/sec
    fs_speed[M_AXIS_INDEX] = 30000;           // micron/sec
    kval_hold[M_AXIS_INDEX] = 3.5;            // volt
    kval_run[M_AXIS_INDEX] = 5.5;             // volt
    kval_acc[M_AXIS_INDEX] = 5.5;             // volt
    kval_dec[M_AXIS_INDEX] = 5.5;             // volt
    ocd_th[M_AXIS_INDEX] = 2200.0;            // ma
    stall_th[M_AXIS_INDEX] = 2000.0;          // ma
    step_sel[M_AXIS_INDEX] = MICROSTEP_1_128; // micro steps per full step
    alarm_en[M_AXIS_INDEX] = 0xFF;            // alarm conditions enable
    config_reg[M_AXIS_INDEX] = 0x2E88;        // ic configuration

    uint8_t axis[NO_OF_MOTORS] = {1, 1, 1, 1};

    Motor_Set_Speed(axis, speed);
    Motor_Set_Min_Speed(axis, min_speed);
    Motor_Set_Max_Speed(axis, max_speed);
    Motor_Set_FS_Speed(axis, fs_speed);
    Motor_Set_ACC(axis, acc);
    Motor_Set_DEC(axis, dec);
    Motor_Set_KVAL_Hold(axis, kval_hold);
    Motor_Set_KVAL_Run(axis, kval_run);
    Motor_Set_KVAL_ACC(axis, kval_acc);
    Motor_Set_KVAL_DEC(axis, kval_dec);
    Motor_Set_OCD_TH(axis, ocd_th);
    Motor_Set_Stall_TH(axis, stall_th);

    L6470_PrepareSetParam(X_AXIS_INDEX, L6470_ALARM_EN_ID, alarm_en[X_AXIS_INDEX]);
    L6470_PrepareSetParam(Y_AXIS_INDEX, L6470_ALARM_EN_ID, alarm_en[X_AXIS_INDEX]);
    L6470_PrepareSetParam(Z_AXIS_INDEX, L6470_ALARM_EN_ID, alarm_en[X_AXIS_INDEX]);
    L6470_PrepareSetParam(M_AXIS_INDEX, L6470_ALARM_EN_ID, alarm_en[X_AXIS_INDEX]);
    L6470_PerformPreparedApplicationCommand();

    Motor_Set_Microsteps(axis, step_sel);
    Motor_Set_Config(axis, config_reg);
}

/**
 * @brief return no of maximum micrometer a motor can move
 * @param axis_index selected stepper
 * @retval  maximum steps
 */
float Motor_Get_Max_Limit(uint8_t axis_index)
{
    return Motor_Max_Limit[axis_index];
}

/**
 * @brief return microstep resolution of motor
 * @param axis_index selected stepper
 * @retval  current steps
 * @note must acquire l6470 mutex before calling this function
 */
uint8_t Motor_Get_Micro_Steps_Per_Full_Step(uint8_t axis_index)
{
    return AbsPos_2_Position(L6470_GetParam(axis_index, L6470_STEP_MODE_ID));
}

/**
 * @brief set no of full steps in 1 micrometer
 * @param axis_index selected stepper
 */
void Motor_Set_Full_Steps_Per_Micron(uint8_t axis_index, float ratio)
{
    Motor_Full_Steps_Per_Micron[axis_index] = ratio;
}

/**
 * @brief return no of full steps in 1 micrometer
 * @param axis_index selected stepper
 * @retval  full steps
 * @note used for conversion to and from speed, acc, dec
 */
float Motor_Get_Full_Steps_Per_Micron(uint8_t axis_index)
{
    return Motor_Full_Steps_Per_Micron[axis_index];
}

/**
 * @brief set no of micro steps in 1 micrometer
 * @param axis_index selected stepper
 */
void Motor_Set_Micro_Steps_Per_Micron(uint8_t axis_index, float ratio)
{
    Motor_Micro_Steps_Per_Micron[axis_index] = ratio;
}

/**
 * @brief return no of micro steps in 1 micrometer
 * @param axis_index selected stepper
 * @retval  maximum steps
 * @note used for conversion to and from move, goto, position
 */
float Motor_Get_Micro_Steps_Per_Micron(uint8_t axis_index)
{
    return Motor_Micro_Steps_Per_Micron[axis_index];
}

/************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************
 ************************************************************************************************************/

/******************************************** L6470 specific ************************************************/

/**
 * @brief read given register of L6470 from all drivers in chain
 * @param reg_id register to read
 * @note must acquire l6470 mutex before calling this funtion
 */
void Motor_Read_Register(eL6470_RegId_t reg_id, uint32_t reg[])
{
    uint8_t reg_len = L6470_Register[reg_id].LengthByte;

    L6470_PrepareGetParam(X_AXIS_INDEX, reg_id);
    L6470_PrepareGetParam(Y_AXIS_INDEX, reg_id);
    L6470_PrepareGetParam(Z_AXIS_INDEX, reg_id);
    L6470_PrepareGetParam(M_AXIS_INDEX, reg_id);

    L6470_PerformPreparedApplicationCommand();

    reg[X_AXIS_INDEX] = L6470_ExtractReturnedData(X_AXIS_INDEX,
                                                  (uint8_t *)L6470_DaisyChainSpiRxStruct,
                                                  reg_len);

    reg[Y_AXIS_INDEX] = L6470_ExtractReturnedData(Y_AXIS_INDEX,
                                                  (uint8_t *)L6470_DaisyChainSpiRxStruct,
                                                  reg_len);

    reg[Z_AXIS_INDEX] = L6470_ExtractReturnedData(Z_AXIS_INDEX,
                                                  (uint8_t *)L6470_DaisyChainSpiRxStruct,
                                                  reg_len);

    reg[M_AXIS_INDEX] = L6470_ExtractReturnedData(M_AXIS_INDEX,
                                                  (uint8_t *)L6470_DaisyChainSpiRxStruct,
                                                  reg_len);
}

/**
 * @brief return index of motor
 * @param name
 * @retval axis
 */
uint8_t Motor_Name_To_Index(char name)
{
    uint8_t temp = 0xFF;
    switch (name)
    {
    case 'x':
        temp = X_AXIS_INDEX;
        break;
    case 'y':
        temp = Y_AXIS_INDEX;
        break;
    case 'z':
        temp = Z_AXIS_INDEX;
        break;
    case 'm':
        temp = M_AXIS_INDEX;
        break;
    }
    return temp;
}

/**
 * @brief move the stepper to given position (relative)
 * @param axis_index axis to move
 * @param micron number of micron to move
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Move(uint8_t axis_index[], float micron[])
{
    float current_pos[NO_OF_MOTORS];

    Motor_Get_Position(current_pos);

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            uint8_t direction = L6470_DIR_FWD_ID;
            float to_move = micron[i];

            if (to_move < 0)
            {
                to_move = -1 * to_move;
                direction = L6470_DIR_REV_ID;
                if (current_pos[i] - to_move < 0)
                {
                    to_move = current_pos[i];
                }
            }
#if (ENABLE_TRAVEL_LIMIT == 1)
            else
            {
                if (current_pos[i] + to_move > Motor_Get_Max_Limit(i))
                {
                    to_move = Motor_Get_Max_Limit(i) - current_pos[i];
                }
            }
#endif

            /** convert micrometer to steps */
            uint32_t steps = Motor_Get_Micro_Steps_Per_Micron(i) * to_move;

            L6470_PrepareMove(i, direction, steps);
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return 1;
}

/**
 * @brief move the stepper to given position (absolute)
 * @param axis_index axis to move
 * @param micron number of micron to move
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Goto(uint8_t axis_index[], float micron[])
{
    float current_pos[NO_OF_MOTORS];

    Motor_Get_Position(current_pos);

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            uint8_t direction = L6470_DIR_FWD_ID;
            float to_move = micron[i];

            if (to_move > 0)
            {
#if (ENABLE_TRAVEL_LIMIT == 1)
                if (to_move > Motor_Get_Max_Limit(i))
                {
                    to_move = Motor_Get_Max_Limit(i);
                }
#endif
                if (to_move < current_pos[i])
                {
                    direction = L6470_DIR_REV_ID; //reverse
                }
            }
            else
            {
                to_move = 0;
            }

            /** convert micrometer to micro steps */
            uint32_t steps = Motor_Get_Micro_Steps_Per_Micron(i) * to_move;

            L6470_PrepareGoToDir(i, direction, steps);
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return 1;
}

/**
 * @brief run the stepper at given speed
 * @param axis_index axis to run
 * @param param running speed (um/sec)
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Run(uint8_t axis_index[], float micron_per_sec[])
{
    float current_pos[NO_OF_MOTORS];

    Motor_Get_Position(current_pos);

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            uint8_t direction = L6470_DIR_FWD_ID;
            float val = micron_per_sec[i];

            if (val < 0)
            {
                val = -1 * val;
                direction = L6470_DIR_REV_ID;
            }

#if (ENABLE_TRAVEL_LIMIT == 1)

            if (current_pos[i] > Motor_Get_Max_Limit(i))
            {
                micron_per_sec = 0;
            }
            if (current_pos[i] < 0)
            {
                micron_per_sec = 0;
            }
#endif
            /** convert micron/sec to setps/sec */
            uint32_t speed = Motor_Get_Full_Steps_Per_Micron(i) * val;

            /** convert steps/sec to appropriate register value */
            speed = Step_s_2_Speed(speed);

            L6470_PrepareRun(i, direction, speed);
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return 1;
}

/**
 * @brief begin homing sequence for given axis
 * @param axis_index axis to home
 * @param param optional speed value of homing cycle
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Home(uint8_t axis_index[], float micron_per_sec[])
{
    uint8_t xreturn = 1;
    uint32_t status[NO_OF_MOTORS];

    /* read l6470 status registers */
    Motor_Read_Register(L6470_STATUS_ID, status);

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = micron_per_sec[i];
            /* speed val must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                if (0 == (int32_t)val)
                {
                    /** if homing speed is not specified use default */
                    val = 10000;
                }

                /** convert micron/sec to steps/sec */
                val = Motor_Get_Full_Steps_Per_Micron(i) * val;

                /** convert value to appropriate register value */
                uint32_t speed = Step_s_2_Speed(val);

                /* read switch status if already pressed, issue release command*/
                /* after release, reset absolute position register*/
                if (status[i] & STATUS_SW_F)
                {
                    L6470_PrepareReleaseSW(i,
                                           L6470_ACT_RST_ID,
                                           L6470_DIR_FWD_ID);
                }
                /* read switch status if not pressed, issue go command*/
                /* after click, reset absolute position register*/
                else
                {
                    L6470_PrepareGoUntil(i,
                                         L6470_ACT_RST_ID,
                                         L6470_DIR_REV_ID,
                                         speed);
                }
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set current speed register in L6470
 * @param axis_index selected axis
 * @param param micron/sec
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_Speed(uint8_t axis_index[], float micron_per_sec[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = micron_per_sec[i];

            /* speed val must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                /** micron/sec to steps/sec */
                val = Motor_Get_Full_Steps_Per_Micron(i) * val;

                if (val > 15625)
                {
                    val = 15625;
                }

                /** convert value to appropriate register value */
                uint32_t reg = Step_s_2_Speed(val);

                L6470_PrepareSetParam(i, L6470_SPEED_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set min speed value in L6470
 * @param axis_index selected axis
 * @param speed
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_Min_Speed(uint8_t axis_index[], float micron_per_sec[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = micron_per_sec[i];

            /* speed val must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                /** micron/sec to steps/sec */
                val = Motor_Get_Full_Steps_Per_Micron(i) * val;

                if (val > 976)
                {
                    val = 976;
                }

                /** convert value to appropriate register value */
                uint32_t reg = Step_s_2_MinSpeed(val);

                L6470_PrepareSetParam(i, L6470_MIN_SPEED_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set max speed value in L6470
 * @param axis_index selected axis
 * @param speed to be set
 * @retval return 1 if success
 */
uint8_t Motor_Set_Max_Speed(uint8_t axis_index[], float micron_per_sec[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = micron_per_sec[i];

            /* speed val must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                /** micron/sec to steps/sec */
                val = Motor_Get_Full_Steps_Per_Micron(i) * val;

                if (val > 15610)
                {
                    val = 15610;
                }

                if (val < 16)
                {
                    val = 16;
                }

                /** convert value to appropriate register value */
                uint32_t reg = Step_s_2_MaxSpeed(val);

                L6470_PrepareSetParam(i, L6470_MAX_SPEED_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set fs speed value in L6470
 * @param axis_index selected axis
 * @param speed to be set
 * @retval return 1 if success
 */
uint8_t Motor_Set_FS_Speed(uint8_t axis_index[], float micron_per_sec[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = micron_per_sec[i];

            /* speed val must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                /** micron/sec to steps/sec */
                val = Motor_Get_Full_Steps_Per_Micron(i) * val;

                if (val > 15625)
                {
                    val = 15625;
                }

                if (val < 8)
                {
                    val = 8;
                }

                /** convert value to appropriate register value */
                uint32_t reg = Step_s_2_FsSpd(val);

                L6470_PrepareSetParam(i, L6470_FS_SPD_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set acc value in L6470
 * @param axis_index selected axis
 * @param acc
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_ACC(uint8_t axis_index[], float micron_s2[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = micron_s2[i];

            /* acc val must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                /** micron/sec/sec to steps/sec/sec */
                val = Motor_Get_Full_Steps_Per_Micron(i) * val;

                if (val > 59590)
                {
                    val = 59590;
                }

                if (val < 15)
                {
                    val = 15;
                }

                /** convert value to appropriate register value */
                uint32_t reg = Step_s2_2_Acc(val);

                L6470_PrepareSetParam(i, L6470_ACC_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set dec value in L6470
 * @param axis_index selected axis
 * @param dec
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_DEC(uint8_t axis_index[], float micron_s2[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = micron_s2[i];
            /* dec val must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                /** micron/sec/sec to setps/sec/sec */
                val = Motor_Get_Full_Steps_Per_Micron(i) * val;

                if (val > 59590)
                {
                    val = 59590;
                }

                if (val < 15)
                {
                    val = 15;
                }

                /** convert value to appropriate register value */
                uint32_t reg = Step_s2_2_Dec(val);

                L6470_PrepareSetParam(i, L6470_DEC_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set kval hold value in L6470
 * @param axis_index selected axis
 * @param kval
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_KVAL_Hold(uint8_t axis_index[], float kval[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = kval[i];
            /* kval must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                if (val > V_MOTOR)
                {
                    val = V_MOTOR;
                }

                /** convert value to appropriate register value */
                uint32_t reg = (uint8_t)((val * 256) / V_MOTOR);

                L6470_PrepareSetParam(i, L6470_KVAL_HOLD_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set kval run value in L6470
 * @param axis_index selected axis
 * @param kval
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_KVAL_Run(uint8_t axis_index[], float kval[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = kval[i];
            /* kval must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                if (val > V_MOTOR)
                {
                    val = V_MOTOR;
                }

                /** convert value to appropriate register value */
                uint32_t reg = (uint8_t)((val * 256) / V_MOTOR);

                L6470_PrepareSetParam(i, L6470_KVAL_RUN_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set kval acc value in L6470
 * @param axis_index selected axis
 * @param kval
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_KVAL_ACC(uint8_t axis_index[], float kval[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = kval[i];
            /* kval must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                if (val > V_MOTOR)
                {
                    val = V_MOTOR;
                }

                /** convert value to appropriate register value */
                uint32_t reg = (uint8_t)((val * 256) / V_MOTOR);

                L6470_PrepareSetParam(i, L6470_KVAL_ACC_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set kval dec value in L6470
 * @param axis_index selected axis
 * @param kval
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_KVAL_DEC(uint8_t axis_index[], float kval[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = kval[i];
            /* kval must be positive*/
            if (val)
            {
                xreturn = 0;
            }
            else
            {
                if (val > V_MOTOR)
                {
                    val = V_MOTOR;
                }

                /** convert value to appropriate register value */
                uint32_t reg = (uint8_t)((val * 256) / V_MOTOR);

                L6470_PrepareSetParam(i, L6470_KVAL_DEC_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set over current threshold value in L6470
 * @param axis_index selected axis
 * @param ocd
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_OCD_TH(uint8_t axis_index[], float ocd[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = ocd[i];

            /* ocd must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                if (val > 6000)
                {
                    val = 6000;
                }

                if (val < 375)
                {
                    val = 375;
                }

                /** convert value to appropriate register value */
                uint32_t reg = mA_2_OcdTh(val);

                L6470_PrepareSetParam(i, L6470_OCD_TH_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set stall threshold value in L6470
 * @param axis_index selected axis
 * @param stall
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_Stall_TH(uint8_t axis_index[], float stall[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = stall[i];

            /* stall must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                if (val > 4000)
                {
                    val = 4000;
                }

                if (val < 31)
                {
                    val = 31;
                }

                /** convert value to appropriate register value */
                uint32_t reg = mA_2_StallTh(val);

                L6470_PrepareSetParam(i, L6470_STALL_TH_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set microstep value in L6470
 * @param axis_index selected axis
 * @param val
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_Microsteps(uint8_t axis_index[], float microsteps[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = microsteps[i];
            /* val must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                uint8_t steps = (uint8_t)val;

                if (steps > 128)
                {
                    steps = 128;
                }

                switch (steps)
                {
                case 0:
                    steps = 0x00;
                    break;
                case 2:
                    steps = 0x01;
                    break;
                case 4:
                    steps = 0x02;
                    break;
                case 8:
                    steps = 0x03;
                    break;
                case 16:
                    steps = 0x04;
                    break;
                case 32:
                    steps = 0x05;
                    break;
                case 64:
                    steps = 0x06;
                    break;
                case 128:
                    steps = 0x07;
                    break;
                default:
                    break;
                }

                L6470_PrepareSetParam(i, L6470_STEP_MODE_ID, steps);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set config register in L6470
 * @param axis_index selected axis
 * @param param value of configuration register
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Set_Config(uint8_t axis_index[], float config[])
{
    uint8_t xreturn = 1;

    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            float val = config[i];
            /* val must be positive*/
            if (val < 0)
            {
                xreturn = 0;
            }
            else
            {
                uint32_t reg = (uint32_t)val;
                L6470_PrepareSetParam(i, L6470_CONFIG_ID, reg);
            }
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return xreturn;
}

/**
 * @brief set given stepper motor in high impedance mode
 * @param axis_index selected axis
 * @param param not used, only axis name is sufficient
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Release(uint8_t axis_index[], float val[])
{
    for (uint8_t i = 0; i < NO_OF_MOTORS; i++)
    {
        if (axis_index[i])
        {
            L6470_PrepareHardHiZ(i);
        }
    }

    L6470_PerformPreparedApplicationCommand();

    return 1;
}

/**
 * @brief return the current position of all axis
 * @param positions[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_Position(float positions[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 position registers */
    Motor_Read_Register(L6470_ABS_POS_ID, reg);

    /* convert register value into steps position then into micrometer*/
    positions[X_AXIS_INDEX] = AbsPos_2_Position(reg[X_AXIS_INDEX]) / Motor_Get_Micro_Steps_Per_Micron(X_AXIS_INDEX);
    positions[Y_AXIS_INDEX] = AbsPos_2_Position(reg[Y_AXIS_INDEX]) / Motor_Get_Micro_Steps_Per_Micron(Y_AXIS_INDEX);
    positions[Z_AXIS_INDEX] = AbsPos_2_Position(reg[Z_AXIS_INDEX]) / Motor_Get_Micro_Steps_Per_Micron(Z_AXIS_INDEX);
    positions[M_AXIS_INDEX] = AbsPos_2_Position(reg[M_AXIS_INDEX]) / Motor_Get_Micro_Steps_Per_Micron(M_AXIS_INDEX);

    return 1;
}

/**
 * @brief return the current speed of all axis
 * @param speed[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_Speed(float speed[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 speed registers */
    Motor_Read_Register(L6470_SPEED_ID, reg);

    /* convert register value into steps/sec then steps/sec to micron/sec */
    speed[X_AXIS_INDEX] = Speed_2_Step_s(reg[X_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(X_AXIS_INDEX);
    speed[Y_AXIS_INDEX] = Speed_2_Step_s(reg[Y_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(Y_AXIS_INDEX);
    speed[Z_AXIS_INDEX] = Speed_2_Step_s(reg[Z_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(Z_AXIS_INDEX);
    speed[M_AXIS_INDEX] = Speed_2_Step_s(reg[M_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(M_AXIS_INDEX);

    return 1;
}

/**
 * @brief return the minimum speed of all axis
 * @param positions[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_Min_Speed(float min_speed[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 min speed registers */
    Motor_Read_Register(L6470_MIN_SPEED_ID, reg);

    /* convert register value into steps/sec then steps/sec to micron/sec */
    min_speed[X_AXIS_INDEX] = MinSpeed_2_Step_s(reg[X_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(X_AXIS_INDEX);
    min_speed[Y_AXIS_INDEX] = MinSpeed_2_Step_s(reg[Y_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(Y_AXIS_INDEX);
    min_speed[Z_AXIS_INDEX] = MinSpeed_2_Step_s(reg[Z_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(Z_AXIS_INDEX);
    min_speed[M_AXIS_INDEX] = MinSpeed_2_Step_s(reg[M_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(M_AXIS_INDEX);

    return 1;
}

/**
 * @brief return the minimum speed of all axis
 * @param positions[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_Max_Speed(float max_speed[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 max speed registers */
    Motor_Read_Register(L6470_MAX_SPEED_ID, reg);

    /* convert register value into steps/sec then into micron/sec */
    max_speed[X_AXIS_INDEX] = MaxSpeed_2_Step_s(reg[X_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(X_AXIS_INDEX);
    max_speed[Y_AXIS_INDEX] = MaxSpeed_2_Step_s(reg[Y_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(Y_AXIS_INDEX);
    max_speed[Z_AXIS_INDEX] = MaxSpeed_2_Step_s(reg[Z_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(Z_AXIS_INDEX);
    max_speed[M_AXIS_INDEX] = MaxSpeed_2_Step_s(reg[M_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(M_AXIS_INDEX);

    return 1;
}

/**
 * @brief return the acceleration of all axis
 * @param positions[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_ACC(float acc[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 acc registers */
    Motor_Read_Register(L6470_ACC_ID, reg);

    /* convert register value into steps/sec/sec then to micron/sec/sec */
    acc[X_AXIS_INDEX] = Acc_2_Step_s2(reg[X_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(X_AXIS_INDEX);
    acc[Y_AXIS_INDEX] = Acc_2_Step_s2(reg[Y_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(Y_AXIS_INDEX);
    acc[Z_AXIS_INDEX] = Acc_2_Step_s2(reg[Z_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(Z_AXIS_INDEX);
    acc[M_AXIS_INDEX] = Acc_2_Step_s2(reg[M_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(M_AXIS_INDEX);

    return 1;
}

/**
 * @brief return the deceleration of all axis
 * @param positions[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_DEC(float dec[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 dec registers */
    Motor_Read_Register(L6470_DEC_ID, reg);

    /* convert register value into steps/sec/sec to micron/sec/sec */
    dec[X_AXIS_INDEX] = Dec_2_Step_s2(reg[X_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(X_AXIS_INDEX);
    dec[Y_AXIS_INDEX] = Dec_2_Step_s2(reg[Y_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(Y_AXIS_INDEX);
    dec[Z_AXIS_INDEX] = Dec_2_Step_s2(reg[Z_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(Z_AXIS_INDEX);
    dec[M_AXIS_INDEX] = Dec_2_Step_s2(reg[M_AXIS_INDEX]) / Motor_Get_Full_Steps_Per_Micron(M_AXIS_INDEX);

    return 1;
}

/**
 * @brief return the kval hold of all axis
 * @param positions[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_KVAL_Hold(float kval[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 kval hold registers */
    Motor_Read_Register(L6470_KVAL_HOLD_ID, reg);

    /* convert register value into volt */
    kval[X_AXIS_INDEX] = reg[X_AXIS_INDEX] * V_MOTOR / 256;
    kval[Y_AXIS_INDEX] = reg[Y_AXIS_INDEX] * V_MOTOR / 256;
    kval[Z_AXIS_INDEX] = reg[Z_AXIS_INDEX] * V_MOTOR / 256;
    kval[M_AXIS_INDEX] = reg[M_AXIS_INDEX] * V_MOTOR / 256;

    return 1;
}

/**
 * @brief return the kval run of all axis
 * @param positions[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_KVAL_Run(float kval[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 kval hold registers */
    Motor_Read_Register(L6470_KVAL_RUN_ID, reg);

    /* convert register value into volt */
    kval[X_AXIS_INDEX] = reg[X_AXIS_INDEX] * V_MOTOR / 256;
    kval[Y_AXIS_INDEX] = reg[Y_AXIS_INDEX] * V_MOTOR / 256;
    kval[Z_AXIS_INDEX] = reg[Z_AXIS_INDEX] * V_MOTOR / 256;
    kval[M_AXIS_INDEX] = reg[M_AXIS_INDEX] * V_MOTOR / 256;

    return 1;
}

/**
 * @brief return the kval acc of all axis
 * @param kval[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_KVAL_ACC(float kval[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 kval acc registers */
    Motor_Read_Register(L6470_KVAL_ACC_ID, reg);

    /* convert register value into volt */
    kval[X_AXIS_INDEX] = reg[X_AXIS_INDEX] * V_MOTOR / 256;
    kval[Y_AXIS_INDEX] = reg[Y_AXIS_INDEX] * V_MOTOR / 256;
    kval[Z_AXIS_INDEX] = reg[Z_AXIS_INDEX] * V_MOTOR / 256;
    kval[M_AXIS_INDEX] = reg[M_AXIS_INDEX] * V_MOTOR / 256;

    return 1;
}

/**
 * @brief return the kval acc of all axis
 * @param kval[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_KVAL_DEC(float kval[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 kval dec registers */
    Motor_Read_Register(L6470_KVAL_DEC_ID, reg);

    /* convert register value into volt */
    kval[X_AXIS_INDEX] = reg[X_AXIS_INDEX] * V_MOTOR / 256;
    kval[Y_AXIS_INDEX] = reg[Y_AXIS_INDEX] * V_MOTOR / 256;
    kval[Z_AXIS_INDEX] = reg[Z_AXIS_INDEX] * V_MOTOR / 256;
    kval[M_AXIS_INDEX] = reg[M_AXIS_INDEX] * V_MOTOR / 256;

    return 1;
}

/**
 * @brief return the ocd threshold of all axis
 * @param ocd_th[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_OCD_TH(float ocd_th[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 ocd th registers */
    Motor_Read_Register(L6470_OCD_TH_ID, reg);

    /* convert register value into milliampere */
    ocd_th[X_AXIS_INDEX] = OcdTh_2_mA(reg[X_AXIS_INDEX]);
    ocd_th[Y_AXIS_INDEX] = OcdTh_2_mA(reg[Y_AXIS_INDEX]);
    ocd_th[Z_AXIS_INDEX] = OcdTh_2_mA(reg[Z_AXIS_INDEX]);
    ocd_th[M_AXIS_INDEX] = OcdTh_2_mA(reg[M_AXIS_INDEX]);

    return 1;
}

/**
 * @brief return the stall threshold of all axis
 * @param stall_th[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_Stall_TH(float stall_th[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 stall th registers */
    Motor_Read_Register(L6470_STALL_TH_ID, reg);

    /* convert register value into milliampere */
    stall_th[X_AXIS_INDEX] = StallTh_2_mA(reg[X_AXIS_INDEX]);
    stall_th[Y_AXIS_INDEX] = StallTh_2_mA(reg[Y_AXIS_INDEX]);
    stall_th[Z_AXIS_INDEX] = StallTh_2_mA(reg[Z_AXIS_INDEX]);
    stall_th[M_AXIS_INDEX] = StallTh_2_mA(reg[M_AXIS_INDEX]);

    return 1;
}

/**
 * @brief return the return microsteps per full step of all axis
 * @param steps[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_Microsteps(float steps[])
{
    uint32_t reg[NO_OF_MOTORS];

    /* read l6470 microstep registers */
    Motor_Read_Register(L6470_STEP_MODE_ID, reg);

    /* convert register value into microstep */
    steps[X_AXIS_INDEX] = pow(2, reg[X_AXIS_INDEX]);
    steps[Y_AXIS_INDEX] = pow(2, reg[Y_AXIS_INDEX]);
    steps[Z_AXIS_INDEX] = pow(2, reg[Z_AXIS_INDEX]);
    steps[M_AXIS_INDEX] = pow(2, reg[M_AXIS_INDEX]);

    return 1;
}

/**
 * @brief return the configuration register of all axis
 * @param reg[]
 * @retval xreturn return 1 if success
 */
uint8_t Motor_Get_Config(uint32_t reg[])
{
    /* read l6470 config registers */
    Motor_Read_Register(L6470_CONFIG_ID, reg);

    return 1;
}

/**
 * @}
 */
/* End of Motor_API_Functions */

/**
 * @}
 */
/* End of Motor_Control */
