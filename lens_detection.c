/**
 * @file    SparkFun_APDS-9960.h
 * @brief   Library for the SparkFun APDS-9960 breakout board
 * @author  Shawn Hymel (SparkFun Electronics)
 *
 * @copyright	This code is public domain but you buy me a beer if you use
 * this and we meet someday (Beerware license).
 *
 * This library interfaces the Avago APDS-9960 to Arduino over I2C. The library
 * relies on the Arduino Wire (I2C) library. to use the library, instantiate an
 * APDS9960 object, call init(), and call the appropriate functions.
 */

/**
 * @file  color_sensor_thread.c
 * @brief implements api related to apds9960 color sensor,based on sparkfun library
 * @author medprime (www.medprimetech.com)
 * @version 0.0.0
 */

/**
 change log
 1.moved functions from turret.c to color_sensor_thread.c
 1.color_sensor_thread.c/h changed to lens_detection.c/h
 */

/** system includes */

/** st includes */
#include "L6470.h"
#include "i2c.h"

/** RTOS includes */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/** app includes */
#include "lens_detection.h"
#include "motor_api.h"
#include "microscope_config.h"
#include "apds9960.h"


extern SemaphoreHandle_t L6470_Access_Mutex;
extern SemaphoreHandle_t L6470_Motion_SEM;
extern SemaphoreHandle_t L6470_Idle_SEM;

static uint8_t Current_Position = 0;

/** list of all possible lenses, depends upon number of slot in turret */
static char *ALL_Lenses[] = {"4X", "10X", "20X", "40X", "100X", "None"};

/** list of lenses currently connected to microscope */
static char *Lenses_On_Scope[NO_OF_LENS];

/** set the active or current position of turret */
void Lens_Set_Current_Position(uint8_t position)
{
    Current_Position = position;
}

/** return active or current position of turret */
uint8_t Lens_Get_Current_Position(void)
{
    return Current_Position;
}

/** assign lens to given turret position */
void Lens_Set_Lens_At_Position(uint8_t position, char *lens)
{
    Lenses_On_Scope[position] = lens;
}

/** return lens at given turret position */
char *Lens_Get_Lens_At_Position(uint8_t position)
{
    return Lenses_On_Scope[position];
}

/** need to properly implement WLED on/off in hardware*/
/** currently using apds9960 interrupt pin in forced low to control WLED*/
static void Color_Sensor_WLED_On(void)
{
    uint8_t temp = APDS9960_IFORCE;
    HAL_I2C_Master_Transmit(&hi2c1, APDS9960_I2C_ADDR, &temp, 1, 10);
}

static void Color_Sensor_WLED_Off(void)
{
    uint8_t temp = APDS9960_AICLEAR;
    HAL_I2C_Master_Transmit(&hi2c1, APDS9960_I2C_ADDR, &temp, 1, 10);
}

void Lens_Color_Sensor_Config(void)
{
    uint8_t temp;

    /** reset all lens to none */
    Lenses_On_Scope[0] = ALL_Lenses[5];
    Lenses_On_Scope[1] = ALL_Lenses[5];
    Lenses_On_Scope[2] = ALL_Lenses[5];
    Lenses_On_Scope[3] = ALL_Lenses[5];

    /** ID Register (0x92)
     *
     */

    /** Enable Register (0x80)
     * p ower on. enable proximity, enable als, enable wait time*
     */
    temp = APDS9960_PON | APDS9960_AEN | APDS9960_PEN | APDS9960_WEN;
    HAL_I2C_Mem_Write(&hi2c1, APDS9960_I2C_ADDR, APDS9960_ENABLE, 1, &temp, 1, 10);

    /** ADC Integration Time Register (0x81)
     *  = 256 – TIME / 2.78 ms
     */
    temp = 240; // 200ms
    HAL_I2C_Mem_Write(&hi2c1, APDS9960_I2C_ADDR, APDS9960_ATIME, 1, &temp, 1, 10);

    /** Wait Time Register (0x83)
     *  = 256 – TIME / 2.78 ms
     */
    temp = 240; // 200ms
    HAL_I2C_Mem_Write(&hi2c1, APDS9960_I2C_ADDR, APDS9960_WTIME, 1, &temp, 1, 10);

    /** Proximity Pulse Count Register (0x8E)
     *
     */
    temp = 0b10000111; //  PPLEN = 16us pulse, PPULSE = 8 pulses
    HAL_I2C_Mem_Write(&hi2c1, APDS9960_I2C_ADDR, APDS9960_PPULSE, 1, &temp, 1, 10);

    /** Control Register One (0x8F)
     *
     */
    temp = 0b01000111; // LDRIVE = 50ma, PGAIN = 2x, AGAIN = 64x
    HAL_I2C_Mem_Write(&hi2c1, APDS9960_I2C_ADDR, APDS9960_CONTROL, 1, &temp, 1, 10);

    /** Status Register (0x93)
     *  Read PVALID and AVALID
     */

    /** RGBC Data Register (0x94 – 0x9B)
     *  RGBC values after AVALID is set
     */

    /**  Proximity Data Register (0x9C)
     *   Proximity value after PVALID is set
     */

    /**  Configuration Three Register (0x9F)
     *   enable proximity photo diodes
     */
    temp = 0b00000000;
    HAL_I2C_Mem_Write(&hi2c1, APDS9960_I2C_ADDR, APDS9960_CONFIG3, 1, &temp, 1, 10);

    /**
     *  enable APDS9960_IFORCE interrupt
     */
    temp = 40;
    HAL_I2C_Mem_Write(&hi2c1, APDS9960_I2C_ADDR, APDS9960_GPENTH, 1, &temp, 1, 10);
}

uint8_t Lens_Read_Proximity(void)
{
    uint8_t temp = 0;

    HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_STATUS, 1, &temp, 1, 10);

    if (temp & 0x02)
    {
        /**PVALID*/
        HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_PDATA, 1, &temp, 1, 10);
    }

    return temp;
}

char *Lens_Read_Current_Lens(void)
{
    uint8_t temp = 0;
    uint8_t lens = 0;
    uint16_t p = 0; //proximity

    /** Status Register (0x93)
     * Read PVALID and AVALID
     */
    HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_STATUS, 1, &temp, 1, 10);

    if (temp & 0x02)
    {
        /***PVALID*/
        HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_PDATA, 1, &temp, 1, 10);
        p = temp;
    }

    if (p > 250)
    {
        uint16_t r = 0; //red
        uint16_t g = 0; //green
        uint16_t b = 0; //blue
        uint16_t a = 0; //ambient

        Color_Sensor_WLED_On();

        /** Atime+Wtime 200ms + 200ms*/
        vTaskDelay(500);

        if (temp & 0x01)
        {
            /**AVALID*/
            HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_CDATAL, 1, &temp, 1, 10);
            a = temp;
            HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_CDATAH, 1, &temp, 1, 10);
            a |= (temp << 8);

            HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_RDATAL, 1, &temp, 1, 10);
            r = temp;
            HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_RDATAH, 1, &temp, 1, 10);
            r |= (temp << 8);

            HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_GDATAL, 1, &temp, 1, 10);
            g = temp;
            HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_GDATAH, 1, &temp, 1, 10);
            g |= (temp << 8);

            HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_BDATAL, 1, &temp, 1, 10);
            b = temp;
            HAL_I2C_Mem_Read(&hi2c1, APDS9960_I2C_ADDR, APDS9960_BDATAH, 1, &temp, 1, 10);
            b |= (temp << 8);
        }

        (void)a;

        if (r > b && b > g)
        {
            /**red 4X*/
            lens = 0;
        }

        if (g > r && r > b)
        {
            /**yellow 10X*/
            lens = 1;
        }

        if (g > b && b > r)
        {
            /**green 20X*/
            lens = 2;
        }

        if (b > g && g > r)
        {
            /**blue 40X*/
            lens = 3;
        }

        if (0)
        {
            /**white 100X*/
            lens = 4;
        }
    }
    else
    {
        /** no lens in proximity*/
        lens = 5;
    }

    Color_Sensor_WLED_Off();

    return ALL_Lenses[lens];
}

/**
 * @note: must acquire l6470 mutex before calling this function
 */
void Lens_Scan_All(void)
{
    /**scan sequence is 2,3,0,1 id sensor is opposite to active position, else 0,1,2,3 */

    /** set active lens at index 2*/
    Lens_Set_Lens_At_Position(2, Lens_Read_Current_Lens());

    /***/
    L6470_Move(M_AXIS_INDEX, L6470_DIR_FWD_ID, MICRO_STEPS_PER_LENS_CHANGE);
    /** wait for motion cmd to finish, semaphore is given from L6470 ISR @see l6470_thread.c */
    xSemaphoreTake(L6470_Idle_SEM, 5000);
    Lens_Set_Lens_At_Position(3, Lens_Read_Current_Lens());

    /***/
    L6470_Move(M_AXIS_INDEX, L6470_DIR_FWD_ID, MICRO_STEPS_PER_LENS_CHANGE);
    /** wait for motion cmd to finish, semaphore is given from L6470 ISR @see l6470_thread.c */
    xSemaphoreTake(L6470_Idle_SEM, 5000);
    Lens_Set_Lens_At_Position(0, Lens_Read_Current_Lens());

    /***/
    L6470_Move(M_AXIS_INDEX, L6470_DIR_FWD_ID, MICRO_STEPS_PER_LENS_CHANGE);
    /** wait for motion cmd to finish, semaphore is given from L6470 ISR @see l6470_thread.c */
    xSemaphoreTake(L6470_Idle_SEM, 5000);
    Lens_Set_Lens_At_Position(1, Lens_Read_Current_Lens());

    /** flip lens index as sensor is opposite to active lens, above scan sequence should be 2,3,0,1 */
    /** this depends upon microscope configuration */
    /** if lens and sensor are on the same side, above scan sequence should be 0,1,2,3 */
    Lens_Set_Current_Position(3);
}
