#include <stdio.h>
#include <stdint.h>

void main()
{
    uint8_t buf[128];
    uint16_t out_written = snprintf(buf, 100, "%s", "hello");
    printf("len of %s = %d, written = %d\n", buf, 4, out_written);

    uint16_t BlockSize = 512;
    printf("%d", sizeof(uint8_t)*BlockSize);
    
}